-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 28, 2016 at 07:52 PM
-- Server version: 5.5.50-0ubuntu0.14.04.1
-- PHP Version: 5.5.37-1+deprecated+dontuse+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hangry`
--

-- --------------------------------------------------------

--
-- Table structure for table `hangry_activities`
--

CREATE TABLE IF NOT EXISTS `hangry_activities` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(10) NOT NULL,
  `context` varchar(128) NOT NULL,
  `user` varchar(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` varchar(32) NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `hangry_activities`
--

INSERT INTO `hangry_activities` (`activity_id`, `domain`, `context`, `user`, `user_id`, `action`, `message`, `status`, `date_added`) VALUES
(11, 'admin', 'staffs', 'staff', 11, 'logged in', '<a href="http://localhost/hangry/admin/staffs/edit?id=11">Nirmal</a> <b>logged</b> in.', 0, '2016-08-28 10:58:59'),
(12, 'admin', 'staffs', 'staff', 11, 'added', 'Nirmal <b>added</b> staff <a href="http://localhost/hangry/admin/staffs/edit"><b>Arpit.</b></a>', 0, '2016-08-28 11:31:19'),
(13, 'admin', 'locations', 'staff', 11, 'added', 'Nirmal <b>added</b> location <a href="http://localhost/hangry/admin/locations/edit?id=11"><b>Mumbai.</b></a>', 0, '2016-08-28 11:34:33'),
(14, 'admin', 'locations', 'staff', 11, 'updated', 'Nirmal <b>updated</b> location <a href="http://localhost/hangry/admin/locations/edit?id=11"><b>Mumbai.</b></a>', 0, '2016-08-28 11:36:57'),
(15, 'admin', 'locations', 'staff', 11, 'updated', 'Nirmal <b>updated</b> location <a href="http://localhost/hangry/admin/locations/edit?id=11"><b>Mumbai.</b></a>', 0, '2016-08-28 11:37:52'),
(16, 'admin', 'themes', 'staff', 11, 'copied', 'Nirmal <b>copied</b> theme <b>TastyIgniter Orange.</b>', 0, '2016-08-28 11:41:12'),
(17, 'module', 'extensions', 'staff', 11, 'updated', 'Nirmal <b>updated</b> module extension <b>Slideshow.</b>', 0, '2016-08-28 11:42:49'),
(18, 'module', 'extensions', 'staff', 11, 'updated', 'Nirmal <b>updated</b> module extension <b>Slideshow.</b>', 0, '2016-08-28 11:44:56'),
(19, 'admin', 'staffs', 'staff', 11, 'logged out', '<a href="http://localhost/hangry/admin/staffs/edit?id=11">Nirmal</a> <b>logged</b> out.', 0, '2016-08-28 16:24:43'),
(20, 'admin', 'staffs', 'staff', 12, 'logged in', '<a href="http://localhost/hangry/admin/staffs/edit?id=12">Arpit</a> <b>logged</b> in.', 0, '2016-08-28 16:24:51'),
(21, 'admin', 'staffs', 'staff', 12, 'logged out', '<a href="http://localhost/hangry/admin/staffs/edit?id=12">Arpit</a> <b>logged</b> out.', 0, '2016-08-28 16:25:30'),
(22, 'admin', 'staffs', 'staff', 11, 'logged in', '<a href="http://localhost/hangry/admin/staffs/edit?id=11">Nirmal</a> <b>logged</b> in.', 0, '2016-08-28 16:25:36'),
(23, 'admin', 'staffs', 'staff', 11, 'logged out', '<a href="http://localhost/hangry/admin/staffs/edit?id=11">Nirmal</a> <b>logged</b> out.', 0, '2016-08-28 16:26:43'),
(24, 'admin', 'staffs', 'staff', 12, 'logged in', '<a href="http://localhost/hangry/admin/staffs/edit?id=12">Arpit</a> <b>logged</b> in.', 0, '2016-08-28 16:26:50'),
(25, 'admin', 'staffs', 'staff', 12, 'logged out', '<a href="http://localhost/hangry/admin/staffs/edit?id=12">Arpit</a> <b>logged</b> out.', 0, '2016-08-28 16:27:40'),
(26, 'admin', 'staffs', 'staff', 11, 'logged in', '<a href="http://localhost/hangry/admin/staffs/edit?id=11">Nirmal</a> <b>logged</b> in.', 0, '2016-08-28 16:27:43'),
(27, 'admin', 'staffs', 'staff', 11, 'logged out', '<a href="http://localhost/hangry/admin/staffs/edit?id=11">Nirmal</a> <b>logged</b> out.', 0, '2016-08-28 16:28:17'),
(28, 'admin', 'staffs', 'staff', 12, 'logged in', '<a href="http://localhost/hangry/admin/staffs/edit?id=12">Arpit</a> <b>logged</b> in.', 0, '2016-08-28 16:28:24'),
(29, 'admin', 'staffs', 'staff', 12, 'logged out', '<a href="http://localhost/hangry/admin/staffs/edit?id=12">Arpit</a> <b>logged</b> out.', 0, '2016-08-28 16:28:36'),
(30, 'admin', 'staffs', 'staff', 11, 'logged in', '<a href="http://localhost/hangry/admin/staffs/edit?id=11">Nirmal</a> <b>logged</b> in.', 0, '2016-08-28 16:28:39'),
(31, 'admin', 'staffs', 'staff', 11, 'logged out', '<a href="http://localhost/hangry/admin/staffs/edit?id=11">Nirmal</a> <b>logged</b> out.', 0, '2016-08-28 16:29:54'),
(32, 'admin', 'staffs', 'staff', 11, 'logged in', '<a href="http://localhost/hangry/admin/staffs/edit?id=11">Nirmal</a> <b>logged</b> in.', 0, '2016-08-28 16:29:56'),
(33, 'admin', 'staffs', 'staff', 11, 'logged out', '<a href="http://localhost/hangry/admin/staffs/edit?id=11">Nirmal</a> <b>logged</b> out.', 0, '2016-08-28 16:29:59'),
(34, 'admin', 'staffs', 'staff', 12, 'logged in', '<a href="http://localhost/hangry/admin/staffs/edit?id=12">Arpit</a> <b>logged</b> in.', 0, '2016-08-28 16:30:07'),
(35, 'admin', 'staffs', 'staff', 12, 'logged out', '<a href="http://localhost/hangry/admin/staffs/edit?id=12">Arpit</a> <b>logged</b> out.', 0, '2016-08-28 16:31:07'),
(36, 'admin', 'staffs', 'staff', 11, 'logged in', '<a href="http://localhost/hangry/admin/staffs/edit?id=11">Nirmal</a> <b>logged</b> in.', 0, '2016-08-28 16:31:09'),
(37, 'admin', 'locations', 'staff', 11, 'updated', 'Nirmal <b>updated</b> location <a href="http://localhost/hangry/admin/locations/edit?id=11"><b>Mumbai.</b></a>', 0, '2016-08-28 16:36:14'),
(38, 'admin', 'locations', 'staff', 11, 'updated', 'Nirmal <b>updated</b> location <a href="http://localhost/hangry/admin/locations/edit?id=11"><b>Mumbai.</b></a>', 0, '2016-08-28 16:36:40'),
(39, 'module', 'extensions', 'staff', 11, 'updated', 'Nirmal <b>updated</b> module extension <b>Local.</b>', 0, '2016-08-28 16:40:29'),
(40, 'module', 'extensions', 'staff', 11, 'updated', 'Nirmal <b>updated</b> module extension <b>Local.</b>', 0, '2016-08-28 16:40:45'),
(41, 'admin', 'menus', 'staff', 11, 'added', 'Nirmal <b>added</b> menu item <a href="http://localhost/hangry/admin/menus/edit?id=11"><b>Lotus Stem Tango.</b></a>', 0, '2016-08-28 18:19:19'),
(42, 'admin', 'locations', 'staff', 11, 'updated', 'Nirmal <b>updated</b> location <a href="http://localhost/hangry/admin/locations/edit?id=11"><b>Mumbai.</b></a>', 0, '2016-08-28 18:43:34'),
(43, 'admin', 'locations', 'staff', 11, 'updated', 'Nirmal <b>updated</b> location <a href="http://localhost/hangry/admin/locations/edit?id=11"><b>Mumbai.</b></a>', 0, '2016-08-28 18:46:23');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_addresses`
--

CREATE TABLE IF NOT EXISTS `hangry_addresses` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(15) NOT NULL,
  `address_1` varchar(128) NOT NULL,
  `address_2` varchar(128) NOT NULL,
  `city` varchar(128) NOT NULL,
  `state` varchar(128) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_banners`
--

CREATE TABLE IF NOT EXISTS `hangry_banners` (
  `banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` char(8) NOT NULL,
  `click_url` varchar(255) NOT NULL,
  `language_id` int(11) NOT NULL,
  `alt_text` varchar(255) NOT NULL,
  `image_code` text NOT NULL,
  `custom_code` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_categories`
--

CREATE TABLE IF NOT EXISTS `hangry_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `parent_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `hangry_categories`
--

INSERT INTO `hangry_categories` (`category_id`, `name`, `description`, `parent_id`, `priority`, `image`, `status`) VALUES
(11, 'Appetizer', 'Start with our fresh and hunger widening appetizers.', 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_countries`
--

CREATE TABLE IF NOT EXISTS `hangry_countries` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(128) NOT NULL,
  `iso_code_2` varchar(2) NOT NULL,
  `iso_code_3` varchar(3) NOT NULL,
  `format` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `flag` varchar(255) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `hangry_countries`
--

INSERT INTO `hangry_countries` (`country_id`, `country_name`, `iso_code_2`, `iso_code_3`, `format`, `status`, `flag`) VALUES
(99, 'India', 'IN', 'IND', '', 1, 'data/flags/in.png');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_coupons`
--

CREATE TABLE IF NOT EXISTS `hangry_coupons` (
  `coupon_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `code` varchar(15) NOT NULL,
  `type` char(1) NOT NULL,
  `discount` decimal(15,4) DEFAULT NULL,
  `min_total` decimal(15,4) DEFAULT NULL,
  `redemptions` int(11) NOT NULL DEFAULT '0',
  `customer_redemptions` int(11) NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date_added` date NOT NULL,
  `validity` char(15) NOT NULL,
  `fixed_date` date DEFAULT NULL,
  `fixed_from_time` time DEFAULT NULL,
  `fixed_to_time` time DEFAULT NULL,
  `period_start_date` date DEFAULT NULL,
  `period_end_date` date DEFAULT NULL,
  `recurring_every` varchar(35) NOT NULL,
  `recurring_from_time` time DEFAULT NULL,
  `recurring_to_time` time DEFAULT NULL,
  `order_restriction` tinyint(4) NOT NULL,
  PRIMARY KEY (`coupon_id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_coupons_history`
--

CREATE TABLE IF NOT EXISTS `hangry_coupons_history` (
  `coupon_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `coupon_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `code` varchar(15) NOT NULL,
  `min_total` decimal(15,4) DEFAULT NULL,
  `amount` decimal(15,4) DEFAULT NULL,
  `date_used` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`coupon_history_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_currencies`
--

CREATE TABLE IF NOT EXISTS `hangry_currencies` (
  `currency_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `currency_name` varchar(32) NOT NULL,
  `currency_code` varchar(3) NOT NULL,
  `currency_symbol` varchar(3) NOT NULL,
  `currency_rate` decimal(15,8) NOT NULL,
  `symbol_position` tinyint(4) NOT NULL,
  `thousand_sign` char(1) NOT NULL,
  `decimal_sign` char(1) NOT NULL,
  `decimal_position` char(1) NOT NULL,
  `iso_alpha2` varchar(2) NOT NULL,
  `iso_alpha3` varchar(3) NOT NULL,
  `iso_numeric` int(11) NOT NULL,
  `flag` varchar(6) NOT NULL,
  `currency_status` int(1) NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`currency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `hangry_currencies`
--

INSERT INTO `hangry_currencies` (`currency_id`, `country_id`, `currency_name`, `currency_code`, `currency_symbol`, `currency_rate`, `symbol_position`, `thousand_sign`, `decimal_sign`, `decimal_position`, `iso_alpha2`, `iso_alpha3`, `iso_numeric`, `flag`, `currency_status`, `date_modified`) VALUES
(1, 1, 'Afghani', 'AFN', '؋', 0.00000000, 0, ',', '.', '2', 'AF', 'AFG', 4, 'AF.png', 0, '2016-08-27 09:55:30'),
(2, 2, 'Lek', 'ALL', 'Lek', 0.00000000, 0, ',', '.', '2', 'AL', 'ALB', 8, 'AL.png', 0, '2016-08-27 09:55:30'),
(3, 3, 'Dinar', 'DZD', 'د.ج', 0.00000000, 0, ',', '.', '2', 'DZ', 'DZA', 12, 'DZ.png', 0, '2016-08-27 09:55:30'),
(4, 4, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'AS', 'ASM', 16, 'AS.png', 0, '2016-08-27 09:55:30'),
(5, 5, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'AD', 'AND', 20, 'AD.png', 0, '2016-08-27 09:55:30'),
(6, 6, 'Kwanza', 'AOA', 'Kz', 0.00000000, 0, ',', '.', '2', 'AO', 'AGO', 24, 'AO.png', 0, '2016-08-27 09:55:30'),
(7, 7, 'Dollar', 'XCD', '$', 0.00000000, 0, ',', '.', '2', 'AI', 'AIA', 660, 'AI.png', 0, '2016-08-27 09:55:30'),
(8, 8, 'Antarctican', 'AQD', 'A$', 0.00000000, 0, ',', '.', '2', 'AQ', 'ATA', 10, 'AQ.png', 0, '2016-08-27 09:55:30'),
(9, 9, 'Dollar', 'XCD', '$', 0.00000000, 0, ',', '.', '2', 'AG', 'ATG', 28, 'AG.png', 0, '2016-08-27 09:55:30'),
(10, 10, 'Peso', 'ARS', '$', 0.00000000, 0, ',', '.', '2', 'AR', 'ARG', 32, 'AR.png', 0, '2016-08-27 09:55:30'),
(11, 11, 'Dram', 'AMD', 'դր.', 0.00000000, 0, ',', '.', '2', 'AM', 'ARM', 51, 'AM.png', 0, '2016-08-27 09:55:30'),
(12, 12, 'Guilder', 'AWG', 'ƒ', 0.00000000, 0, ',', '.', '2', 'AW', 'ABW', 533, 'AW.png', 0, '2016-08-27 09:55:30'),
(13, 13, 'Dollar', 'AUD', '$', 0.00000000, 0, ',', '.', '2', 'AU', 'AUS', 36, 'AU.png', 0, '2016-08-27 09:55:30'),
(14, 14, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'AT', 'AUT', 40, 'AT.png', 0, '2016-08-27 09:55:30'),
(15, 15, 'Manat', 'AZN', 'ман', 0.00000000, 0, ',', '.', '2', 'AZ', 'AZE', 31, 'AZ.png', 0, '2016-08-27 09:55:30'),
(16, 16, 'Dollar', 'BSD', '$', 0.00000000, 0, ',', '.', '2', 'BS', 'BHS', 44, 'BS.png', 0, '2016-08-27 09:55:30'),
(17, 17, 'Dinar', 'BHD', '.د.', 0.00000000, 0, ',', '.', '2', 'BH', 'BHR', 48, 'BH.png', 0, '2016-08-27 09:55:30'),
(18, 18, 'Taka', 'BDT', '৳', 0.00000000, 0, ',', '.', '2', 'BD', 'BGD', 50, 'BD.png', 0, '2016-08-27 09:55:30'),
(19, 19, 'Dollar', 'BBD', '$', 0.00000000, 0, ',', '.', '2', 'BB', 'BRB', 52, 'BB.png', 0, '2016-08-27 09:55:30'),
(20, 20, 'Ruble', 'BYR', 'p.', 0.00000000, 0, ',', '.', '2', 'BY', 'BLR', 112, 'BY.png', 0, '2016-08-27 09:55:30'),
(21, 21, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'BE', 'BEL', 56, 'BE.png', 0, '2016-08-27 09:55:30'),
(22, 22, 'Dollar', 'BZD', 'BZ$', 0.00000000, 0, ',', '.', '2', 'BZ', 'BLZ', 84, 'BZ.png', 0, '2016-08-27 09:55:30'),
(23, 23, 'Franc', 'XOF', '', 0.00000000, 0, ',', '.', '2', 'BJ', 'BEN', 204, 'BJ.png', 0, '2016-08-27 09:55:30'),
(24, 24, 'Dollar', 'BMD', '$', 0.00000000, 0, ',', '.', '2', 'BM', 'BMU', 60, 'BM.png', 0, '2016-08-27 09:55:30'),
(25, 25, 'Ngultrum', 'BTN', 'Nu.', 0.00000000, 0, ',', '.', '2', 'BT', 'BTN', 64, 'BT.png', 0, '2016-08-27 09:55:30'),
(26, 26, 'Boliviano', 'BOB', '$b', 0.00000000, 0, ',', '.', '2', 'BO', 'BOL', 68, 'BO.png', 0, '2016-08-27 09:55:30'),
(27, 27, 'Marka', 'BAM', 'KM', 0.00000000, 0, ',', '.', '2', 'BA', 'BIH', 70, 'BA.png', 0, '2016-08-27 09:55:30'),
(28, 28, 'Pula', 'BWP', 'P', 0.00000000, 0, ',', '.', '2', 'BW', 'BWA', 72, 'BW.png', 0, '2016-08-27 09:55:30'),
(29, 29, 'Krone', 'NOK', 'kr', 0.00000000, 0, ',', '.', '2', 'BV', 'BVT', 74, 'BV.png', 0, '2016-08-27 09:55:30'),
(30, 30, 'Real', 'BRL', 'R$', 0.00000000, 0, ',', '.', '2', 'BR', 'BRA', 76, 'BR.png', 0, '2016-08-27 09:55:30'),
(31, 31, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'IO', 'IOT', 86, 'IO.png', 0, '2016-08-27 09:55:30'),
(32, 231, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'VG', 'VGB', 92, 'VG.png', 0, '2016-08-27 09:55:30'),
(33, 32, 'Dollar', 'BND', '$', 0.00000000, 0, ',', '.', '2', 'BN', 'BRN', 96, 'BN.png', 0, '2016-08-27 09:55:30'),
(34, 33, 'Lev', 'BGN', 'лв', 0.00000000, 0, ',', '.', '2', 'BG', 'BGR', 100, 'BG.png', 0, '2016-08-27 09:55:30'),
(35, 34, 'Franc', 'XOF', '', 0.00000000, 0, ',', '.', '2', 'BF', 'BFA', 854, 'BF.png', 0, '2016-08-27 09:55:30'),
(36, 35, 'Franc', 'BIF', 'Fr', 0.00000000, 0, ',', '.', '2', 'BI', 'BDI', 108, 'BI.png', 0, '2016-08-27 09:55:30'),
(37, 36, 'Riels', 'KHR', '៛', 0.00000000, 0, ',', '.', '2', 'KH', 'KHM', 116, 'KH.png', 0, '2016-08-27 09:55:30'),
(38, 37, 'Franc', 'XAF', 'FCF', 0.00000000, 0, ',', '.', '2', 'CM', 'CMR', 120, 'CM.png', 0, '2016-08-27 09:55:30'),
(39, 38, 'Dollar', 'CAD', '$', 0.00000000, 0, ',', '.', '2', 'CA', 'CAN', 124, 'CA.png', 0, '2016-08-27 09:55:30'),
(40, 39, 'Escudo', 'CVE', '', 0.00000000, 0, ',', '.', '2', 'CV', 'CPV', 132, 'CV.png', 0, '2016-08-27 09:55:30'),
(41, 40, 'Dollar', 'KYD', '$', 0.00000000, 0, ',', '.', '2', 'KY', 'CYM', 136, 'KY.png', 0, '2016-08-27 09:55:30'),
(42, 41, 'Franc', 'XAF', 'FCF', 0.00000000, 0, ',', '.', '2', 'CF', 'CAF', 140, 'CF.png', 0, '2016-08-27 09:55:30'),
(43, 42, 'Franc', 'XAF', '', 0.00000000, 0, ',', '.', '2', 'TD', 'TCD', 148, 'TD.png', 0, '2016-08-27 09:55:30'),
(44, 43, 'Peso', 'CLP', '', 0.00000000, 0, ',', '.', '2', 'CL', 'CHL', 152, 'CL.png', 0, '2016-08-27 09:55:30'),
(45, 44, 'Yuan Renminbi', 'CNY', '¥', 0.00000000, 0, ',', '.', '2', 'CN', 'CHN', 156, 'CN.png', 0, '2016-08-27 09:55:30'),
(46, 45, 'Dollar', 'AUD', '$', 0.00000000, 0, ',', '.', '2', 'CX', 'CXR', 162, 'CX.png', 0, '2016-08-27 09:55:30'),
(47, 46, 'Dollar', 'AUD', '$', 0.00000000, 0, ',', '.', '2', 'CC', 'CCK', 166, 'CC.png', 0, '2016-08-27 09:55:30'),
(48, 47, 'Peso', 'COP', '$', 0.00000000, 0, ',', '.', '2', 'CO', 'COL', 170, 'CO.png', 0, '2016-08-27 09:55:30'),
(49, 48, 'Franc', 'KMF', '', 0.00000000, 0, ',', '.', '2', 'KM', 'COM', 174, 'KM.png', 0, '2016-08-27 09:55:30'),
(50, 50, 'Dollar', 'NZD', '$', 0.00000000, 0, ',', '.', '2', 'CK', 'COK', 184, 'CK.png', 0, '2016-08-27 09:55:30'),
(51, 51, 'Colon', 'CRC', '₡', 0.00000000, 0, ',', '.', '2', 'CR', 'CRI', 188, 'CR.png', 0, '2016-08-27 09:55:30'),
(52, 53, 'Kuna', 'HRK', 'kn', 0.00000000, 0, ',', '.', '2', 'HR', 'HRV', 191, 'HR.png', 0, '2016-08-27 09:55:30'),
(53, 54, 'Peso', 'CUP', '₱', 0.00000000, 0, ',', '.', '2', 'CU', 'CUB', 192, 'CU.png', 0, '2016-08-27 09:55:30'),
(54, 55, 'Pound', 'CYP', '', 0.00000000, 0, ',', '.', '2', 'CY', 'CYP', 196, 'CY.png', 0, '2016-08-27 09:55:30'),
(55, 56, 'Koruna', 'CZK', 'Kč', 0.00000000, 0, ',', '.', '2', 'CZ', 'CZE', 203, 'CZ.png', 0, '2016-08-27 09:55:30'),
(56, 49, 'Franc', 'CDF', 'FC', 0.00000000, 0, ',', '.', '2', 'CD', 'COD', 180, 'CD.png', 0, '2016-08-27 09:55:30'),
(57, 57, 'Krone', 'DKK', 'kr', 0.00000000, 0, ',', '.', '2', 'DK', 'DNK', 208, 'DK.png', 0, '2016-08-27 09:55:30'),
(58, 58, 'Franc', 'DJF', '', 0.00000000, 0, ',', '.', '2', 'DJ', 'DJI', 262, 'DJ.png', 0, '2016-08-27 09:55:30'),
(59, 59, 'Dollar', 'XCD', '$', 0.00000000, 0, ',', '.', '2', 'DM', 'DMA', 212, 'DM.png', 0, '2016-08-27 09:55:30'),
(60, 60, 'Peso', 'DOP', 'RD$', 0.00000000, 0, ',', '.', '2', 'DO', 'DOM', 214, 'DO.png', 0, '2016-08-27 09:55:30'),
(61, 61, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'TL', 'TLS', 626, 'TL.png', 0, '2016-08-27 09:55:30'),
(62, 62, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'EC', 'ECU', 218, 'EC.png', 0, '2016-08-27 09:55:30'),
(63, 63, 'Pound', 'EGP', '£', 0.00000000, 0, ',', '.', '2', 'EG', 'EGY', 818, 'EG.png', 0, '2016-08-27 09:55:30'),
(64, 64, 'Colone', 'SVC', '$', 0.00000000, 0, ',', '.', '2', 'SV', 'SLV', 222, 'SV.png', 0, '2016-08-27 09:55:30'),
(65, 65, 'Franc', 'XAF', 'FCF', 0.00000000, 0, ',', '.', '2', 'GQ', 'GNQ', 226, 'GQ.png', 0, '2016-08-27 09:55:30'),
(66, 66, 'Nakfa', 'ERN', 'Nfk', 0.00000000, 0, ',', '.', '2', 'ER', 'ERI', 232, 'ER.png', 0, '2016-08-27 09:55:30'),
(67, 67, 'Kroon', 'EEK', 'kr', 0.00000000, 0, ',', '.', '2', 'EE', 'EST', 233, 'EE.png', 0, '2016-08-27 09:55:30'),
(68, 68, 'Birr', 'ETB', '', 0.00000000, 0, ',', '.', '2', 'ET', 'ETH', 231, 'ET.png', 0, '2016-08-27 09:55:30'),
(69, 69, 'Pound', 'FKP', '£', 0.00000000, 0, ',', '.', '2', 'FK', 'FLK', 238, 'FK.png', 0, '2016-08-27 09:55:30'),
(70, 70, 'Krone', 'DKK', 'kr', 0.00000000, 0, ',', '.', '2', 'FO', 'FRO', 234, 'FO.png', 0, '2016-08-27 09:55:30'),
(71, 71, 'Dollar', 'FJD', '$', 0.00000000, 0, ',', '.', '2', 'FJ', 'FJI', 242, 'FJ.png', 0, '2016-08-27 09:55:30'),
(72, 72, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'FI', 'FIN', 246, 'FI.png', 0, '2016-08-27 09:55:30'),
(73, 73, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'FR', 'FRA', 250, 'FR.png', 0, '2016-08-27 09:55:30'),
(74, 75, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'GF', 'GUF', 254, 'GF.png', 0, '2016-08-27 09:55:30'),
(75, 76, 'Franc', 'XPF', '', 0.00000000, 0, ',', '.', '2', 'PF', 'PYF', 258, 'PF.png', 0, '2016-08-27 09:55:30'),
(76, 77, 'Euro  ', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'TF', 'ATF', 260, 'TF.png', 0, '2016-08-27 09:55:30'),
(77, 78, 'Franc', 'XAF', 'FCF', 0.00000000, 0, ',', '.', '2', 'GA', 'GAB', 266, 'GA.png', 0, '2016-08-27 09:55:30'),
(78, 79, 'Dalasi', 'GMD', 'D', 0.00000000, 0, ',', '.', '2', 'GM', 'GMB', 270, 'GM.png', 0, '2016-08-27 09:55:30'),
(79, 80, 'Lari', 'GEL', '', 0.00000000, 0, ',', '.', '2', 'GE', 'GEO', 268, 'GE.png', 0, '2016-08-27 09:55:30'),
(80, 81, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'DE', 'DEU', 276, 'DE.png', 0, '2016-08-27 09:55:30'),
(81, 82, 'Cedi', 'GHC', '¢', 0.00000000, 0, ',', '.', '2', 'GH', 'GHA', 288, 'GH.png', 0, '2016-08-27 09:55:30'),
(82, 83, 'Pound', 'GIP', '£', 0.00000000, 0, ',', '.', '2', 'GI', 'GIB', 292, 'GI.png', 0, '2016-08-27 09:55:30'),
(83, 84, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'GR', 'GRC', 300, 'GR.png', 0, '2016-08-27 09:55:30'),
(84, 85, 'Krone', 'DKK', 'kr', 0.00000000, 0, ',', '.', '2', 'GL', 'GRL', 304, 'GL.png', 0, '2016-08-27 09:55:30'),
(85, 86, 'Dollar', 'XCD', '$', 0.00000000, 0, ',', '.', '2', 'GD', 'GRD', 308, 'GD.png', 0, '2016-08-27 09:55:30'),
(86, 87, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'GP', 'GLP', 312, 'GP.png', 0, '2016-08-27 09:55:30'),
(87, 88, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'GU', 'GUM', 316, 'GU.png', 0, '2016-08-27 09:55:30'),
(88, 89, 'Quetzal', 'GTQ', 'Q', 0.00000000, 0, ',', '.', '2', 'GT', 'GTM', 320, 'GT.png', 0, '2016-08-27 09:55:30'),
(89, 90, 'Franc', 'GNF', '', 0.00000000, 0, ',', '.', '2', 'GN', 'GIN', 324, 'GN.png', 0, '2016-08-27 09:55:30'),
(90, 91, 'Franc', 'XOF', '', 0.00000000, 0, ',', '.', '2', 'GW', 'GNB', 624, 'GW.png', 0, '2016-08-27 09:55:30'),
(91, 92, 'Dollar', 'GYD', '$', 0.00000000, 0, ',', '.', '2', 'GY', 'GUY', 328, 'GY.png', 0, '2016-08-27 09:55:30'),
(92, 93, 'Gourde', 'HTG', 'G', 0.00000000, 0, ',', '.', '2', 'HT', 'HTI', 332, 'HT.png', 0, '2016-08-27 09:55:30'),
(93, 94, 'Dollar', 'AUD', '$', 0.00000000, 0, ',', '.', '2', 'HM', 'HMD', 334, 'HM.png', 0, '2016-08-27 09:55:30'),
(94, 95, 'Lempira', 'HNL', 'L', 0.00000000, 0, ',', '.', '2', 'HN', 'HND', 340, 'HN.png', 0, '2016-08-27 09:55:30'),
(95, 96, 'Dollar', 'HKD', '$', 0.00000000, 0, ',', '.', '2', 'HK', 'HKG', 344, 'HK.png', 0, '2016-08-27 09:55:30'),
(96, 97, 'Forint', 'HUF', 'Ft', 0.00000000, 0, ',', '.', '2', 'HU', 'HUN', 348, 'HU.png', 0, '2016-08-27 09:55:30'),
(97, 98, 'Krona', 'ISK', 'kr', 0.00000000, 0, ',', '.', '2', 'IS', 'ISL', 352, 'IS.png', 0, '2016-08-27 09:55:30'),
(98, 99, 'Rupee', 'INR', '₹', 0.00000000, 0, ',', '.', '2', 'IN', 'IND', 356, 'IN.png', 1, '2016-08-27 09:55:30'),
(99, 100, 'Rupiah', 'IDR', 'Rp', 0.00000000, 0, ',', '.', '2', 'ID', 'IDN', 360, 'ID.png', 0, '2016-08-27 09:55:30'),
(100, 101, 'Rial', 'IRR', '﷼', 0.00000000, 0, ',', '.', '2', 'IR', 'IRN', 364, 'IR.png', 0, '2016-08-27 09:55:30'),
(101, 102, 'Dinar', 'IQD', '', 0.00000000, 0, ',', '.', '2', 'IQ', 'IRQ', 368, 'IQ.png', 0, '2016-08-27 09:55:30'),
(102, 103, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'IE', 'IRL', 372, 'IE.png', 0, '2016-08-27 09:55:30'),
(103, 104, 'Shekel', 'ILS', '₪', 0.00000000, 0, ',', '.', '2', 'IL', 'ISR', 376, 'IL.png', 0, '2016-08-27 09:55:30'),
(104, 105, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'IT', 'ITA', 380, 'IT.png', 0, '2016-08-27 09:55:30'),
(105, 52, 'Franc', 'XOF', '', 0.00000000, 0, ',', '.', '2', 'CI', 'CIV', 384, 'CI.png', 0, '2016-08-27 09:55:30'),
(106, 106, 'Dollar', 'JMD', '$', 0.00000000, 0, ',', '.', '2', 'JM', 'JAM', 388, 'JM.png', 0, '2016-08-27 09:55:30'),
(107, 107, 'Yen', 'JPY', '¥', 0.00000000, 0, ',', '.', '2', 'JP', 'JPN', 392, 'JP.png', 0, '2016-08-27 09:55:30'),
(108, 108, 'Dinar', 'JOD', '', 0.00000000, 0, ',', '.', '2', 'JO', 'JOR', 400, 'JO.png', 0, '2016-08-27 09:55:30'),
(109, 109, 'Tenge', 'KZT', 'лв', 0.00000000, 0, ',', '.', '2', 'KZ', 'KAZ', 398, 'KZ.png', 0, '2016-08-27 09:55:30'),
(110, 110, 'Shilling', 'KES', '', 0.00000000, 0, ',', '.', '2', 'KE', 'KEN', 404, 'KE.png', 0, '2016-08-27 09:55:30'),
(111, 111, 'Dollar', 'AUD', '$', 0.00000000, 0, ',', '.', '2', 'KI', 'KIR', 296, 'KI.png', 0, '2016-08-27 09:55:30'),
(112, 114, 'Dinar', 'KWD', 'د.ك', 0.00000000, 0, ',', '.', '2', 'KW', 'KWT', 414, 'KW.png', 0, '2016-08-27 09:55:30'),
(113, 115, 'Som', 'KGS', 'лв', 0.00000000, 0, ',', '.', '2', 'KG', 'KGZ', 417, 'KG.png', 0, '2016-08-27 09:55:30'),
(114, 116, 'Kip', 'LAK', '₭', 0.00000000, 0, ',', '.', '2', 'LA', 'LAO', 418, 'LA.png', 0, '2016-08-27 09:55:30'),
(115, 117, 'Lat', 'LVL', 'Ls', 0.00000000, 0, ',', '.', '2', 'LV', 'LVA', 428, 'LV.png', 0, '2016-08-27 09:55:30'),
(116, 118, 'Pound', 'LBP', '£', 0.00000000, 0, ',', '.', '2', 'LB', 'LBN', 422, 'LB.png', 0, '2016-08-27 09:55:30'),
(117, 119, 'Loti', 'LSL', 'L', 0.00000000, 0, ',', '.', '2', 'LS', 'LSO', 426, 'LS.png', 0, '2016-08-27 09:55:30'),
(118, 120, 'Dollar', 'LRD', '$', 0.00000000, 0, ',', '.', '2', 'LR', 'LBR', 430, 'LR.png', 0, '2016-08-27 09:55:30'),
(119, 121, 'Dinar', 'LYD', 'ل.د', 0.00000000, 0, ',', '.', '2', 'LY', 'LBY', 434, 'LY.png', 0, '2016-08-27 09:55:30'),
(120, 122, 'Franc', 'CHF', 'CHF', 0.00000000, 0, ',', '.', '2', 'LI', 'LIE', 438, 'LI.png', 0, '2016-08-27 09:55:30'),
(121, 123, 'Litas', 'LTL', 'Lt', 0.00000000, 0, ',', '.', '2', 'LT', 'LTU', 440, 'LT.png', 0, '2016-08-27 09:55:30'),
(122, 124, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'LU', 'LUX', 442, 'LU.png', 0, '2016-08-27 09:55:30'),
(123, 125, 'Pataca', 'MOP', 'MOP', 0.00000000, 0, ',', '.', '2', 'MO', 'MAC', 446, 'MO.png', 0, '2016-08-27 09:55:30'),
(124, 140, 'Denar', 'MKD', 'ден', 0.00000000, 0, ',', '.', '2', 'MK', 'MKD', 807, 'MK.png', 0, '2016-08-27 09:55:30'),
(125, 127, 'Ariary', 'MGA', 'Ar', 0.00000000, 0, ',', '.', '2', 'MG', 'MDG', 450, 'MG.png', 0, '2016-08-27 09:55:30'),
(126, 128, 'Kwacha', 'MWK', 'MK', 0.00000000, 0, ',', '.', '2', 'MW', 'MWI', 454, 'MW.png', 0, '2016-08-27 09:55:30'),
(127, 129, 'Ringgit', 'MYR', 'RM', 0.00000000, 0, ',', '.', '2', 'MY', 'MYS', 458, 'MY.png', 0, '2016-08-27 09:55:30'),
(128, 130, 'Rufiyaa', 'MVR', 'Rf', 0.00000000, 0, ',', '.', '2', 'MV', 'MDV', 462, 'MV.png', 0, '2016-08-27 09:55:30'),
(129, 131, 'Franc', 'XOF', 'MAF', 0.00000000, 0, ',', '.', '2', 'ML', 'MLI', 466, 'ML.png', 0, '2016-08-27 09:55:30'),
(130, 132, 'Lira', 'MTL', 'Lm', 0.00000000, 0, ',', '.', '2', 'MT', 'MLT', 470, 'MT.png', 0, '2016-08-27 09:55:30'),
(131, 133, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'MH', 'MHL', 584, 'MH.png', 0, '2016-08-27 09:55:30'),
(132, 134, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'MQ', 'MTQ', 474, 'MQ.png', 0, '2016-08-27 09:55:30'),
(133, 135, 'Ouguiya', 'MRO', 'UM', 0.00000000, 0, ',', '.', '2', 'MR', 'MRT', 478, 'MR.png', 0, '2016-08-27 09:55:30'),
(134, 136, 'Rupee', 'MUR', '₨', 0.00000000, 0, ',', '.', '2', 'MU', 'MUS', 480, 'MU.png', 0, '2016-08-27 09:55:30'),
(135, 137, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'YT', 'MYT', 175, 'YT.png', 0, '2016-08-27 09:55:30'),
(136, 138, 'Peso', 'MXN', '$', 0.00000000, 0, ',', '.', '2', 'MX', 'MEX', 484, 'MX.png', 0, '2016-08-27 09:55:30'),
(137, 139, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'FM', 'FSM', 583, 'FM.png', 0, '2016-08-27 09:55:30'),
(138, 140, 'Leu', 'MDL', 'MDL', 0.00000000, 0, ',', '.', '2', 'MD', 'MDA', 498, 'MD.png', 0, '2016-08-27 09:55:30'),
(139, 141, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'MC', 'MCO', 492, 'MC.png', 0, '2016-08-27 09:55:30'),
(140, 142, 'Tugrik', 'MNT', '₮', 0.00000000, 0, ',', '.', '2', 'MN', 'MNG', 496, 'MN.png', 0, '2016-08-27 09:55:30'),
(141, 143, 'Dollar', 'XCD', '$', 0.00000000, 0, ',', '.', '2', 'MS', 'MSR', 500, 'MS.png', 0, '2016-08-27 09:55:30'),
(142, 144, 'Dirham', 'MAD', '', 0.00000000, 0, ',', '.', '2', 'MA', 'MAR', 504, 'MA.png', 0, '2016-08-27 09:55:30'),
(143, 145, 'Meticail', 'MZN', 'MT', 0.00000000, 0, ',', '.', '2', 'MZ', 'MOZ', 508, 'MZ.png', 0, '2016-08-27 09:55:30'),
(144, 146, 'Kyat', 'MMK', 'K', 0.00000000, 0, ',', '.', '2', 'MM', 'MMR', 104, 'MM.png', 0, '2016-08-27 09:55:30'),
(145, 147, 'Dollar', 'NAD', '$', 0.00000000, 0, ',', '.', '2', 'NA', 'NAM', 516, 'NA.png', 0, '2016-08-27 09:55:30'),
(146, 148, 'Dollar', 'AUD', '$', 0.00000000, 0, ',', '.', '2', 'NR', 'NRU', 520, 'NR.png', 0, '2016-08-27 09:55:30'),
(147, 149, 'Rupee', 'NPR', '₨', 0.00000000, 0, ',', '.', '2', 'NP', 'NPL', 524, 'NP.png', 0, '2016-08-27 09:55:30'),
(148, 150, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'NL', 'NLD', 528, 'NL.png', 0, '2016-08-27 09:55:30'),
(149, 151, 'Guilder', 'ANG', 'ƒ', 0.00000000, 0, ',', '.', '2', 'AN', 'ANT', 530, 'AN.png', 0, '2016-08-27 09:55:30'),
(150, 152, 'Franc', 'XPF', '', 0.00000000, 0, ',', '.', '2', 'NC', 'NCL', 540, 'NC.png', 0, '2016-08-27 09:55:30'),
(151, 153, 'Dollar', 'NZD', '$', 0.00000000, 0, ',', '.', '2', 'NZ', 'NZL', 554, 'NZ.png', 0, '2016-08-27 09:55:30'),
(152, 154, 'Cordoba', 'NIO', 'C$', 0.00000000, 0, ',', '.', '2', 'NI', 'NIC', 558, 'NI.png', 0, '2016-08-27 09:55:30'),
(153, 155, 'Franc', 'XOF', '', 0.00000000, 0, ',', '.', '2', 'NE', 'NER', 562, 'NE.png', 0, '2016-08-27 09:55:30'),
(154, 156, 'Naira', 'NGN', '₦', 0.00000000, 0, ',', '.', '2', 'NG', 'NGA', 566, 'NG.png', 0, '2016-08-27 09:55:30'),
(155, 157, 'Dollar', 'NZD', '$', 0.00000000, 0, ',', '.', '2', 'NU', 'NIU', 570, 'NU.png', 0, '2016-08-27 09:55:30'),
(156, 158, 'Dollar', 'AUD', '$', 0.00000000, 0, ',', '.', '2', 'NF', 'NFK', 574, 'NF.png', 0, '2016-08-27 09:55:30'),
(157, 112, 'Won', 'KPW', '₩', 0.00000000, 0, ',', '.', '2', 'KP', 'PRK', 408, 'KP.png', 0, '2016-08-27 09:55:30'),
(158, 159, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'MP', 'MNP', 580, 'MP.png', 0, '2016-08-27 09:55:30'),
(159, 160, 'Krone', 'NOK', 'kr', 0.00000000, 0, ',', '.', '2', 'NO', 'NOR', 578, 'NO.png', 0, '2016-08-27 09:55:30'),
(160, 161, 'Rial', 'OMR', '﷼', 0.00000000, 0, ',', '.', '2', 'OM', 'OMN', 512, 'OM.png', 0, '2016-08-27 09:55:30'),
(161, 162, 'Rupee', 'PKR', '₨', 0.00000000, 0, ',', '.', '2', 'PK', 'PAK', 586, 'PK.png', 0, '2016-08-27 09:55:30'),
(162, 163, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'PW', 'PLW', 585, 'PW.png', 0, '2016-08-27 09:55:30'),
(163, 0, 'Shekel', 'ILS', '₪', 0.00000000, 0, ',', '.', '2', 'PS', 'PSE', 275, 'PS.png', 0, '2016-08-27 09:55:30'),
(164, 164, 'Balboa', 'PAB', 'B/.', 0.00000000, 0, ',', '.', '2', 'PA', 'PAN', 591, 'PA.png', 0, '2016-08-27 09:55:30'),
(165, 165, 'Kina', 'PGK', '', 0.00000000, 0, ',', '.', '2', 'PG', 'PNG', 598, 'PG.png', 0, '2016-08-27 09:55:30'),
(166, 166, 'Guarani', 'PYG', 'Gs', 0.00000000, 0, ',', '.', '2', 'PY', 'PRY', 600, 'PY.png', 0, '2016-08-27 09:55:30'),
(167, 167, 'Sol', 'PEN', 'S/.', 0.00000000, 0, ',', '.', '2', 'PE', 'PER', 604, 'PE.png', 0, '2016-08-27 09:55:30'),
(168, 168, 'Peso', 'PHP', 'Php', 0.00000000, 0, ',', '.', '2', 'PH', 'PHL', 608, 'PH.png', 0, '2016-08-27 09:55:30'),
(169, 169, 'Dollar', 'NZD', '$', 0.00000000, 0, ',', '.', '2', 'PN', 'PCN', 612, 'PN.png', 0, '2016-08-27 09:55:30'),
(170, 170, 'Zloty', 'PLN', 'zł', 0.00000000, 0, ',', '.', '2', 'PL', 'POL', 616, 'PL.png', 0, '2016-08-27 09:55:30'),
(171, 171, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'PT', 'PRT', 620, 'PT.png', 0, '2016-08-27 09:55:30'),
(172, 172, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'PR', 'PRI', 630, 'PR.png', 0, '2016-08-27 09:55:30'),
(173, 173, 'Rial', 'QAR', '﷼', 0.00000000, 0, ',', '.', '2', 'QA', 'QAT', 634, 'QA.png', 0, '2016-08-27 09:55:30'),
(174, 49, 'Franc', 'XAF', 'FCF', 0.00000000, 0, ',', '.', '2', 'CG', 'COG', 178, 'CG.png', 0, '2016-08-27 09:55:30'),
(175, 174, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'RE', 'REU', 638, 'RE.png', 0, '2016-08-27 09:55:30'),
(176, 175, 'Leu', 'RON', 'lei', 0.00000000, 0, ',', '.', '2', 'RO', 'ROU', 642, 'RO.png', 0, '2016-08-27 09:55:30'),
(177, 176, 'Ruble', 'RUB', 'руб', 0.00000000, 0, ',', '.', '2', 'RU', 'RUS', 643, 'RU.png', 0, '2016-08-27 09:55:30'),
(178, 177, 'Franc', 'RWF', '', 0.00000000, 0, ',', '.', '2', 'RW', 'RWA', 646, 'RW.png', 0, '2016-08-27 09:55:30'),
(179, 179, 'Pound', 'SHP', '£', 0.00000000, 0, ',', '.', '2', 'SH', 'SHN', 654, 'SH.png', 0, '2016-08-27 09:55:30'),
(180, 178, 'Dollar', 'XCD', '$', 0.00000000, 0, ',', '.', '2', 'KN', 'KNA', 659, 'KN.png', 0, '2016-08-27 09:55:30'),
(181, 179, 'Dollar', 'XCD', '$', 0.00000000, 0, ',', '.', '2', 'LC', 'LCA', 662, 'LC.png', 0, '2016-08-27 09:55:30'),
(182, 180, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'PM', 'SPM', 666, 'PM.png', 0, '2016-08-27 09:55:30'),
(183, 180, 'Dollar', 'XCD', '$', 0.00000000, 0, ',', '.', '2', 'VC', 'VCT', 670, 'VC.png', 0, '2016-08-27 09:55:30'),
(184, 181, 'Tala', 'WST', 'WS$', 0.00000000, 0, ',', '.', '2', 'WS', 'WSM', 882, 'WS.png', 0, '2016-08-27 09:55:30'),
(185, 182, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'SM', 'SMR', 674, 'SM.png', 0, '2016-08-27 09:55:30'),
(186, 183, 'Dobra', 'STD', 'Db', 0.00000000, 0, ',', '.', '2', 'ST', 'STP', 678, 'ST.png', 0, '2016-08-27 09:55:30'),
(187, 184, 'Rial', 'SAR', '﷼', 0.00000000, 0, ',', '.', '2', 'SA', 'SAU', 682, 'SA.png', 0, '2016-08-27 09:55:30'),
(188, 185, 'Franc', 'XOF', '', 0.00000000, 0, ',', '.', '2', 'SN', 'SEN', 686, 'SN.png', 0, '2016-08-27 09:55:30'),
(189, 142, 'Dinar', 'RSD', 'Дин', 0.00000000, 0, ',', '.', '2', 'CS', 'SCG', 891, 'CS.png', 0, '2016-08-27 09:55:30'),
(190, 186, 'Rupee', 'SCR', '₨', 0.00000000, 0, ',', '.', '2', 'SC', 'SYC', 690, 'SC.png', 0, '2016-08-27 09:55:30'),
(191, 187, 'Leone', 'SLL', 'Le', 0.00000000, 0, ',', '.', '2', 'SL', 'SLE', 694, 'SL.png', 0, '2016-08-27 09:55:30'),
(192, 188, 'Dollar', 'SGD', '$', 0.00000000, 0, ',', '.', '2', 'SG', 'SGP', 702, 'SG.png', 0, '2016-08-27 09:55:30'),
(193, 189, 'Koruna', 'SKK', 'Sk', 0.00000000, 0, ',', '.', '2', 'SK', 'SVK', 703, 'SK.png', 0, '2016-08-27 09:55:30'),
(194, 190, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'SI', 'SVN', 705, 'SI.png', 0, '2016-08-27 09:55:30'),
(195, 191, 'Dollar', 'SBD', '$', 0.00000000, 0, ',', '.', '2', 'SB', 'SLB', 90, 'SB.png', 0, '2016-08-27 09:55:30'),
(196, 192, 'Shilling', 'SOS', 'S', 0.00000000, 0, ',', '.', '2', 'SO', 'SOM', 706, 'SO.png', 0, '2016-08-27 09:55:30'),
(197, 193, 'Rand', 'ZAR', 'R', 0.00000000, 0, ',', '.', '2', 'ZA', 'ZAF', 710, 'ZA.png', 0, '2016-08-27 09:55:30'),
(198, 113, 'Pound', 'GBP', '£', 0.00000000, 0, ',', '.', '2', 'GS', 'SGS', 239, 'GS.png', 0, '2016-08-27 09:55:30'),
(199, 194, 'Won', 'KRW', '₩', 0.00000000, 0, ',', '.', '2', 'KR', 'KOR', 410, 'KR.png', 0, '2016-08-27 09:55:30'),
(200, 195, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'ES', 'ESP', 724, 'ES.png', 0, '2016-08-27 09:55:30'),
(201, 196, 'Rupee', 'LKR', '₨', 0.00000000, 0, ',', '.', '2', 'LK', 'LKA', 144, 'LK.png', 0, '2016-08-27 09:55:30'),
(202, 199, 'Dinar', 'SDD', '', 0.00000000, 0, ',', '.', '2', 'SD', 'SDN', 736, 'SD.png', 0, '2016-08-27 09:55:30'),
(203, 200, 'Dollar', 'SRD', '$', 0.00000000, 0, ',', '.', '2', 'SR', 'SUR', 740, 'SR.png', 0, '2016-08-27 09:55:30'),
(204, 0, 'Krone', 'NOK', 'kr', 0.00000000, 0, ',', '.', '2', 'SJ', 'SJM', 744, 'SJ.png', 0, '2016-08-27 09:55:30'),
(205, 202, 'Lilangeni', 'SZL', '', 0.00000000, 0, ',', '.', '2', 'SZ', 'SWZ', 748, 'SZ.png', 0, '2016-08-27 09:55:30'),
(206, 203, 'Krona', 'SEK', 'kr', 0.00000000, 0, ',', '.', '2', 'SE', 'SWE', 752, 'SE.png', 0, '2016-08-27 09:55:30'),
(207, 204, 'Franc', 'CHF', 'CHF', 0.00000000, 0, ',', '.', '2', 'CH', 'CHE', 756, 'CH.png', 0, '2016-08-27 09:55:30'),
(208, 205, 'Pound', 'SYP', '£', 0.00000000, 0, ',', '.', '2', 'SY', 'SYR', 760, 'SY.png', 0, '2016-08-27 09:55:30'),
(209, 206, 'Dollar', 'TWD', 'NT$', 0.00000000, 0, ',', '.', '2', 'TW', 'TWN', 158, 'TW.png', 0, '2016-08-27 09:55:30'),
(210, 207, 'Somoni', 'TJS', '', 0.00000000, 0, ',', '.', '2', 'TJ', 'TJK', 762, 'TJ.png', 0, '2016-08-27 09:55:30'),
(211, 208, 'Shilling', 'TZS', '', 0.00000000, 0, ',', '.', '2', 'TZ', 'TZA', 834, 'TZ.png', 0, '2016-08-27 09:55:30'),
(212, 209, 'Baht', 'THB', '฿', 0.00000000, 0, ',', '.', '2', 'TH', 'THA', 764, 'TH.png', 0, '2016-08-27 09:55:30'),
(213, 210, 'Franc', 'XOF', '', 0.00000000, 0, ',', '.', '2', 'TG', 'TGO', 768, 'TG.png', 0, '2016-08-27 09:55:30'),
(214, 211, 'Dollar', 'NZD', '$', 0.00000000, 0, ',', '.', '2', 'TK', 'TKL', 772, 'TK.png', 0, '2016-08-27 09:55:30'),
(215, 212, 'Pa''anga', 'TOP', 'T$', 0.00000000, 0, ',', '.', '2', 'TO', 'TON', 776, 'TO.png', 0, '2016-08-27 09:55:30'),
(216, 213, 'Dollar', 'TTD', 'TT$', 0.00000000, 0, ',', '.', '2', 'TT', 'TTO', 780, 'TT.png', 0, '2016-08-27 09:55:30'),
(217, 214, 'Dinar', 'TND', '', 0.00000000, 0, ',', '.', '2', 'TN', 'TUN', 788, 'TN.png', 0, '2016-08-27 09:55:30'),
(218, 215, 'Lira', 'TRY', 'YTL', 0.00000000, 0, ',', '.', '2', 'TR', 'TUR', 792, 'TR.png', 0, '2016-08-27 09:55:30'),
(219, 216, 'Manat', 'TMM', 'm', 0.00000000, 0, ',', '.', '2', 'TM', 'TKM', 795, 'TM.png', 0, '2016-08-27 09:55:30'),
(220, 217, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'TC', 'TCA', 796, 'TC.png', 0, '2016-08-27 09:55:30'),
(221, 218, 'Dollar', 'AUD', '$', 0.00000000, 0, ',', '.', '2', 'TV', 'TUV', 798, 'TV.png', 0, '2016-08-27 09:55:30'),
(222, 232, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'VI', 'VIR', 850, 'VI.png', 0, '2016-08-27 09:55:30'),
(223, 219, 'Shilling', 'UGX', '', 0.00000000, 0, ',', '.', '2', 'UG', 'UGA', 800, 'UG.png', 0, '2016-08-27 09:55:30'),
(224, 220, 'Hryvnia', 'UAH', '₴', 0.00000000, 0, ',', '.', '2', 'UA', 'UKR', 804, 'UA.png', 0, '2016-08-27 09:55:30'),
(225, 221, 'Dirham', 'AED', '', 0.00000000, 0, ',', '.', '2', 'AE', 'ARE', 784, 'AE.png', 0, '2016-08-27 09:55:30'),
(226, 222, 'Pound', 'GBP', '£', 0.00000000, 0, ',', '.', '2', 'GB', 'GBR', 826, 'GB.png', 0, '2016-08-27 09:55:30'),
(227, 223, 'Dollar', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'US', 'USA', 840, 'US.png', 0, '2016-08-27 09:55:30'),
(228, 224, 'Dollar ', 'USD', '$', 0.00000000, 0, ',', '.', '2', 'UM', 'UMI', 581, 'UM.png', 0, '2016-08-27 09:55:30'),
(229, 225, 'Peso', 'UYU', '$U', 0.00000000, 0, ',', '.', '2', 'UY', 'URY', 858, 'UY.png', 0, '2016-08-27 09:55:30'),
(230, 226, 'Som', 'UZS', 'лв', 0.00000000, 0, ',', '.', '2', 'UZ', 'UZB', 860, 'UZ.png', 0, '2016-08-27 09:55:30'),
(231, 227, 'Vatu', 'VUV', 'Vt', 0.00000000, 0, ',', '.', '2', 'VU', 'VUT', 548, 'VU.png', 0, '2016-08-27 09:55:30'),
(232, 228, 'Euro', 'EUR', '€', 0.00000000, 0, ',', '.', '2', 'VA', 'VAT', 336, 'VA.png', 0, '2016-08-27 09:55:30'),
(233, 229, 'Bolivar', 'VEF', 'Bs', 0.00000000, 0, ',', '.', '2', 'VE', 'VEN', 862, 'VE.png', 0, '2016-08-27 09:55:30'),
(234, 230, 'Dong', 'VND', '₫', 0.00000000, 0, ',', '.', '2', 'VN', 'VNM', 704, 'VN.png', 0, '2016-08-27 09:55:30'),
(235, 233, 'Franc', 'XPF', '', 0.00000000, 0, ',', '.', '2', 'WF', 'WLF', 876, 'WF.png', 0, '2016-08-27 09:55:30'),
(236, 234, 'Dirham', 'MAD', '', 0.00000000, 0, ',', '.', '2', 'EH', 'ESH', 732, 'EH.png', 0, '2016-08-27 09:55:30'),
(237, 235, 'Rial', 'YER', '﷼', 0.00000000, 0, ',', '.', '2', 'YE', 'YEM', 887, 'YE.png', 0, '2016-08-27 09:55:30'),
(238, 238, 'Kwacha', 'ZMK', 'ZK', 0.00000000, 0, ',', '.', '2', 'ZM', 'ZMB', 894, 'ZM.png', 0, '2016-08-27 09:55:30'),
(239, 239, 'Dollar', 'ZWD', 'Z$', 0.00000000, 0, ',', '.', '2', 'ZW', 'ZWE', 716, 'ZW.png', 0, '2016-08-27 09:55:30');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_customers`
--

CREATE TABLE IF NOT EXISTS `hangry_customers` (
  `customer_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `address_id` int(11) NOT NULL,
  `security_question_id` int(11) NOT NULL,
  `security_answer` varchar(32) NOT NULL,
  `newsletter` tinyint(1) NOT NULL,
  `customer_group_id` int(11) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `date_added` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `cart` text NOT NULL,
  PRIMARY KEY (`customer_id`,`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_customers_online`
--

CREATE TABLE IF NOT EXISTS `hangry_customers_online` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `access_type` varchar(128) NOT NULL,
  `browser` varchar(128) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `request_uri` text NOT NULL,
  `referrer_uri` text NOT NULL,
  `date_added` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  `user_agent` text NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `hangry_customers_online`
--

INSERT INTO `hangry_customers_online` (`activity_id`, `customer_id`, `access_type`, `browser`, `ip_address`, `country_code`, `request_uri`, `referrer_uri`, `date_added`, `status`, `user_agent`) VALUES
(11, 0, 'browser', 'Chrome', '::1', '0', '', 'setup/success', '2016-08-28 10:58:45', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(12, 0, 'browser', 'Chrome', '::1', '0', 'locations', '', '2016-08-28 11:08:46', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(13, 0, 'browser', 'Chrome', '::1', '0', 'locations', '', '2016-08-28 11:14:03', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(14, 0, 'browser', 'Chrome', '::1', '0', 'locations', '', '2016-08-28 11:26:36', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(15, 0, 'browser', 'Chrome', '::1', '0', 'locations', '', '2016-08-28 11:41:18', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(16, 0, 'browser', 'Chrome', '::1', '0', '', '', '2016-08-28 11:44:32', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(17, 0, 'browser', 'Chrome', '::1', '0', 'reservation', '', '2016-08-28 16:17:17', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(18, 0, 'browser', 'Chrome', '::1', '0', 'register', 'home', '2016-08-28 16:22:22', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(19, 0, 'browser', 'Chrome', '::1', '0', '', 'register', '2016-08-28 16:32:03', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(20, 0, 'browser', 'Chrome', '::1', '0', '', 'register', '2016-08-28 16:34:07', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(21, 0, 'browser', 'Chrome', '::1', '0', 'locations', '', '2016-08-28 16:36:16', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(22, 0, 'browser', 'Chrome', '::1', '0', '', 'locations?sort_by=newest', '2016-08-28 16:38:46', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(23, 0, 'browser', 'Chrome', '::1', '0', '', 'locations?sort_by=newest', '2016-08-28 16:40:47', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(24, 0, 'browser', 'Chrome', '::1', '0', '', 'locations?sort_by=newest', '2016-08-28 17:55:23', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(25, 0, 'browser', 'Chrome', '::1', '0', '', '', '2016-08-28 17:57:34', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(26, 0, 'browser', 'Chrome', '::1', '0', '', 'register', '2016-08-28 17:59:46', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(27, 0, 'browser', 'Chrome', '::1', '0', '', 'register', '2016-08-28 18:01:47', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(28, 0, 'browser', 'Chrome', '::1', '0', '', 'register', '2016-08-28 18:05:24', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(29, 0, 'browser', 'Chrome', '::1', '0', 'local_module/local_module/search', '', '2016-08-28 18:07:34', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(30, 0, 'browser', 'Chrome', '::1', '0', '', 'register', '2016-08-28 18:11:07', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(31, 0, 'browser', 'Chrome', '::1', '0', 'locations', '', '2016-08-28 18:13:09', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(32, 0, 'browser', 'Chrome', '::1', '0', 'local?location_id=11', 'locations?search=mumbai', '2016-08-28 18:16:44', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(33, 0, 'browser', 'Chrome', '::1', '0', 'local?location_id=11', 'locations?search=mumbai', '2016-08-28 18:19:22', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36'),
(34, 0, 'browser', 'Chrome', '::1', '0', 'local?location_id=11', 'local?location_id=11', '2016-08-28 18:36:31', 0, 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_customer_groups`
--

CREATE TABLE IF NOT EXISTS `hangry_customer_groups` (
  `customer_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(32) NOT NULL,
  `description` text NOT NULL,
  `approval` tinyint(1) NOT NULL,
  PRIMARY KEY (`customer_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `hangry_customer_groups`
--

INSERT INTO `hangry_customer_groups` (`customer_group_id`, `group_name`, `description`, `approval`) VALUES
(11, 'Default', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_extensions`
--

CREATE TABLE IF NOT EXISTS `hangry_extensions` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `data` text NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `version` varchar(11) NOT NULL DEFAULT '1.0.0',
  PRIMARY KEY (`extension_id`),
  UNIQUE KEY `type` (`type`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `hangry_extensions`
--

INSERT INTO `hangry_extensions` (`extension_id`, `type`, `name`, `data`, `serialized`, `status`, `title`, `version`) VALUES
(11, 'module', 'account_module', 'a:1:{s:7:"layouts";a:1:{i:0;a:4:{s:9:"layout_id";s:2:"11";s:8:"position";s:4:"left";s:8:"priority";s:1:"1";s:6:"status";s:1:"1";}}}', 1, 1, 'Account', '1.0.0'),
(12, 'module', 'local_module', 'a:3:{s:20:"location_search_mode";s:5:"multi";s:12:"use_location";s:1:"0";s:6:"status";s:1:"1";}', 1, 1, 'Local', '1.0.0'),
(13, 'module', 'categories_module', 'a:1:{s:7:"layouts";a:1:{i:0;a:4:{s:9:"layout_id";s:2:"12";s:8:"position";s:4:"left";s:8:"priority";s:1:"1";s:6:"status";s:1:"1";}}}', 1, 1, 'Categories', '1.0.0'),
(14, 'module', 'cart_module', 'a:3:{s:16:"show_cart_images";s:1:"0";s:13:"cart_images_h";s:0:"";s:13:"cart_images_w";s:0:"";}', 1, 1, 'Cart', '1.0.0'),
(15, 'module', 'reservation_module', 'a:1:{s:7:"layouts";a:1:{i:0;a:4:{s:9:"layout_id";s:2:"16";s:8:"position";s:4:"left";s:8:"priority";s:1:"1";s:6:"status";s:1:"1";}}}', 1, 1, 'Reservation', '1.0.0'),
(16, 'module', 'slideshow', 'a:6:{s:11:"dimension_w";s:4:"1920";s:11:"dimension_h";s:4:"1080";s:6:"effect";s:4:"fade";s:5:"speed";s:3:"500";s:7:"display";s:1:"1";s:6:"slides";a:2:{i:0;a:2:{s:9:"image_src";s:24:"data/Food-Pictures-4.jpg";s:7:"caption";s:0:"";}i:1;a:2:{s:9:"image_src";s:44:"data/Macaroni-In-A-Pasta-Plate-Wallpaper.jpg";s:7:"caption";s:0:"";}}}', 1, 1, 'Slideshow', '1.0.0'),
(18, 'payment', 'cod', 'a:5:{s:4:"name";N;s:11:"order_total";s:4:"0.00";s:12:"order_status";s:2:"11";s:8:"priority";s:1:"1";s:6:"status";s:1:"1";}', 1, 1, 'Cash On Delivery', '1.0.0'),
(20, 'module', 'pages_module', 'a:1:{s:7:"layouts";a:1:{i:0;a:4:{s:9:"layout_id";s:2:"17";s:8:"position";s:5:"right";s:8:"priority";s:1:"1";s:6:"status";s:1:"1";}}}', 1, 1, 'Pages', '1.0.0'),
(21, 'payment', 'paypal_express', 'a:11:{s:8:"priority";s:0:"";s:6:"status";s:1:"0";s:8:"api_mode";s:7:"sandbox";s:8:"api_user";s:0:"";s:8:"api_pass";s:0:"";s:13:"api_signature";s:0:"";s:10:"api_action";s:4:"sale";s:10:"return_uri";s:24:"paypal_express/authorize";s:10:"cancel_uri";s:21:"paypal_express/cancel";s:11:"order_total";s:4:"0.00";s:12:"order_status";s:2:"11";}', 1, 0, 'PayPal Express', '1.0.0'),
(23, 'theme', 'tastyigniter-orange', '', 1, 1, 'TastyIgniter Orange', '1.0.0'),
(24, 'theme', 'tastyigniter-blue', '', 1, 0, 'TastyIgniter Blue', '1.0.0'),
(25, 'module', 'banners_module', 'a:1:{s:7:"banners";a:1:{i:1;a:3:{s:9:"banner_id";s:1:"1";s:5:"width";s:0:"";s:6:"height";s:0:"";}}}', 1, 0, 'Banners', '1.0.0'),
(26, 'theme', 'tastyigniter-orange-child', 'a:20:{s:14:"display_crumbs";s:1:"0";s:15:"hide_admin_link";s:1:"1";s:16:"ga_tracking_code";s:0:"";s:4:"font";a:5:{s:6:"family";s:25:""Oxygen",Arial,sans-serif";s:6:"weight";s:6:"normal";s:5:"style";s:6:"normal";s:4:"size";s:2:"13";s:5:"color";s:7:"#333333";}s:9:"menu_font";a:5:{s:6:"family";s:25:""Oxygen",Arial,sans-serif";s:6:"weight";s:6:"normal";s:5:"style";s:6:"normal";s:4:"size";s:2:"16";s:5:"color";s:7:"#ed561a";}s:4:"body";a:6:{s:10:"background";s:7:"#ffffff";s:5:"image";s:0:"";s:7:"display";s:7:"contain";s:10:"foreground";s:7:"#ffffff";s:5:"color";s:7:"#ed561a";s:6:"border";s:7:"#dddddd";}s:4:"link";a:2:{s:5:"color";s:7:"#337ab7";s:5:"hover";s:7:"#23527c";}s:7:"heading";a:6:{s:10:"background";s:0:"";s:5:"image";s:0:"";s:7:"display";s:7:"contain";s:5:"color";s:7:"#333333";s:11:"under_image";s:0:"";s:12:"under_height";s:2:"50";}s:6:"button";a:6:{s:7:"default";a:3:{s:10:"background";s:7:"#e7e7e7";s:5:"hover";s:7:"#e7e7e7";s:4:"font";s:7:"#333333";}s:7:"primary";a:3:{s:10:"background";s:7:"#428bca";s:5:"hover";s:7:"#428bca";s:4:"font";s:7:"#ffffff";}s:7:"success";a:3:{s:10:"background";s:7:"#5cb85c";s:5:"hover";s:7:"#5cb85c";s:4:"font";s:7:"#ffffff";}s:4:"info";a:3:{s:10:"background";s:7:"#5bc0de";s:5:"hover";s:7:"#5bc0de";s:4:"font";s:7:"#ffffff";}s:7:"warning";a:3:{s:10:"background";s:7:"#f0ad4e";s:5:"hover";s:7:"#f0ad4e";s:4:"font";s:7:"#ffffff";}s:6:"danger";a:3:{s:10:"background";s:7:"#d9534f";s:6:"border";s:7:"#d9534f";s:4:"font";s:7:"#ffffff";}}s:7:"sidebar";a:5:{s:10:"background";s:7:"#ffffff";s:5:"image";s:0:"";s:7:"display";s:7:"contain";s:4:"font";s:7:"#484848";s:6:"border";s:7:"#eeeeee";}s:6:"header";a:5:{s:10:"background";s:7:"#ffffff";s:5:"image";s:0:"";s:7:"display";s:7:"contain";s:19:"dropdown_background";s:7:"#ed561a";s:5:"color";s:7:"#ed561a";}s:10:"logo_image";s:0:"";s:9:"logo_text";s:0:"";s:11:"logo_height";s:2:"40";s:16:"logo_padding_top";s:2:"10";s:19:"logo_padding_bottom";s:2:"10";s:7:"favicon";s:0:"";s:6:"footer";a:8:{s:10:"background";s:7:"#ed561a";s:5:"image";s:0:"";s:7:"display";s:7:"contain";s:17:"bottom_background";s:7:"#ed561a";s:12:"bottom_image";s:0:"";s:14:"bottom_display";s:7:"contain";s:12:"footer_color";s:7:"#ffffff";s:19:"bottom_footer_color";s:7:"#ffffff";}s:6:"social";a:12:{s:8:"facebook";s:1:"#";s:7:"twitter";s:1:"#";s:6:"google";s:1:"#";s:7:"youtube";s:1:"#";s:5:"vimeo";s:0:"";s:8:"linkedin";s:0:"";s:9:"pinterest";s:0:"";s:6:"tumblr";s:0:"";s:6:"flickr";s:0:"";s:9:"instagram";s:0:"";s:8:"dribbble";s:0:"";s:10:"foursquare";s:0:"";}s:13:"custom_script";a:3:{s:3:"css";s:127:"#main-header .navbar-nav > li > a:focus, #main-header .navbar-nav > li > a:hover, .local-box-fluid h2 {color: #fff !important;}";s:4:"head";s:0:"";s:6:"footer";s:0:"";}}', 1, 1, 'Hangry Hours Theme', '2.0'),
(27, 'cart_total', 'cart_total', 'a:5:{s:8:"priority";s:1:"1";s:4:"name";s:10:"cart_total";s:5:"title";s:9:"Sub Total";s:11:"admin_title";s:9:"Sub Total";s:6:"status";s:1:"1";}', 1, 1, 'Sub Total', '1.0.0'),
(28, 'cart_total', 'coupon', 'a:5:{s:8:"priority";s:1:"3";s:4:"name";s:6:"coupon";s:5:"title";s:15:"Coupon {coupon}";s:11:"admin_title";s:15:"Coupon {coupon}";s:6:"status";s:1:"1";}', 1, 1, 'Coupon', '1.0.0'),
(29, 'cart_total', 'delivery', 'a:5:{s:8:"priority";s:1:"4";s:4:"name";s:8:"delivery";s:5:"title";s:8:"Delivery";s:11:"admin_title";s:8:"Delivery";s:6:"status";s:1:"1";}', 1, 1, 'Delivery', '1.0.0'),
(30, 'cart_total', 'taxes', 'a:5:{s:8:"priority";s:1:"5";s:4:"name";s:5:"taxes";s:5:"title";s:9:"VAT {tax}";s:11:"admin_title";s:9:"VAT {tax}";s:6:"status";s:1:"1";}', 1, 1, 'VAT', '1.0.0'),
(31, 'cart_total', 'order_total', 'a:5:{s:8:"priority";s:1:"6";s:4:"name";s:11:"order_total";s:5:"title";s:11:"Order Total";s:11:"admin_title";s:11:"Order Total";s:6:"status";s:1:"1";}', 1, 1, 'Order Total', '1.0.0');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_languages`
--

CREATE TABLE IF NOT EXISTS `hangry_languages` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(7) NOT NULL,
  `name` varchar(32) NOT NULL,
  `image` varchar(32) NOT NULL,
  `idiom` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `can_delete` tinyint(1) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `hangry_languages`
--

INSERT INTO `hangry_languages` (`language_id`, `code`, `name`, `image`, `idiom`, `status`, `can_delete`) VALUES
(11, 'en', 'English', 'data/flags/gb.png', 'english', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_layouts`
--

CREATE TABLE IF NOT EXISTS `hangry_layouts` (
  `layout_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`layout_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `hangry_layouts`
--

INSERT INTO `hangry_layouts` (`layout_id`, `name`) VALUES
(11, 'Home'),
(12, 'Menus'),
(13, 'Checkout'),
(15, 'Account'),
(16, 'Reservation'),
(17, 'Page'),
(18, 'Local'),
(19, 'Locations');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_layout_modules`
--

CREATE TABLE IF NOT EXISTS `hangry_layout_modules` (
  `layout_module_id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) NOT NULL,
  `module_code` varchar(128) NOT NULL,
  `partial` varchar(32) NOT NULL,
  `priority` int(11) NOT NULL,
  `options` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`layout_module_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `hangry_layout_modules`
--

INSERT INTO `hangry_layout_modules` (`layout_module_id`, `layout_id`, `module_code`, `partial`, `priority`, `options`, `status`) VALUES
(60, 17, 'pages_module', 'content_right', 1, '', 1),
(65, 11, 'slideshow', 'content_top', 1, '', 1),
(66, 11, 'local_module', 'content_top', 2, '', 1),
(67, 15, 'account_module', 'content_left', 1, '', 1),
(68, 12, 'local_module', 'content_top', 1, '', 1),
(69, 12, 'categories_module', 'content_left', 1, '', 1),
(70, 12, 'cart_module', 'content_right', 1, '', 1),
(71, 13, 'local_module', 'content_top', 1, '', 1),
(72, 13, 'cart_module', 'content_right', 1, '', 1),
(73, 16, 'reservation_module', 'content_top', 1, '', 1),
(74, 18, 'local_module', 'content_top', 1, '', 1),
(75, 18, 'categories_module', 'content_left', 1, '', 1),
(76, 18, 'cart_module', 'content_right', 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_layout_routes`
--

CREATE TABLE IF NOT EXISTS `hangry_layout_routes` (
  `layout_route_id` int(11) NOT NULL AUTO_INCREMENT,
  `layout_id` int(11) NOT NULL,
  `uri_route` varchar(128) NOT NULL,
  PRIMARY KEY (`layout_route_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=107 ;

--
-- Dumping data for table `hangry_layout_routes`
--

INSERT INTO `hangry_layout_routes` (`layout_route_id`, `layout_id`, `uri_route`) VALUES
(19, 13, 'checkout'),
(41, 16, 'reservation'),
(44, 12, 'menus'),
(59, 11, 'home'),
(70, 18, 'local'),
(71, 19, 'locations'),
(72, 17, 'pages'),
(100, 15, 'account/account'),
(101, 15, 'account/details'),
(102, 15, 'account/address'),
(103, 15, 'account/orders'),
(104, 15, 'account/reservations'),
(105, 15, 'account/inbox'),
(106, 15, 'account/reviews');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_locations`
--

CREATE TABLE IF NOT EXISTS `hangry_locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(32) NOT NULL,
  `location_email` varchar(96) NOT NULL,
  `description` text NOT NULL,
  `location_address_1` varchar(128) NOT NULL,
  `location_address_2` varchar(128) NOT NULL,
  `location_city` varchar(128) NOT NULL,
  `location_state` varchar(128) NOT NULL,
  `location_postcode` varchar(10) NOT NULL,
  `location_country_id` int(11) NOT NULL,
  `location_telephone` varchar(32) NOT NULL,
  `location_lat` float(10,6) NOT NULL,
  `location_lng` float(10,6) NOT NULL,
  `location_radius` int(11) NOT NULL,
  `offer_delivery` tinyint(1) NOT NULL,
  `offer_collection` tinyint(1) NOT NULL,
  `delivery_time` int(11) NOT NULL,
  `last_order_time` int(11) NOT NULL,
  `reservation_time_interval` int(11) NOT NULL,
  `reservation_stay_time` int(11) NOT NULL,
  `location_status` tinyint(1) NOT NULL,
  `collection_time` int(11) NOT NULL,
  `options` text NOT NULL,
  `location_image` varchar(255) NOT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `hangry_locations`
--

INSERT INTO `hangry_locations` (`location_id`, `location_name`, `location_email`, `description`, `location_address_1`, `location_address_2`, `location_city`, `location_state`, `location_postcode`, `location_country_id`, `location_telephone`, `location_lat`, `location_lng`, `location_radius`, `offer_delivery`, `offer_collection`, `delivery_time`, `last_order_time`, `reservation_time_interval`, `reservation_stay_time`, `location_status`, `collection_time`, `options`, `location_image`) VALUES
(11, 'Mumbai', 'arpit.tolia@gmail.com', '', 'Demo add 1', 'Demo add 2', 'Mumbai', 'Maharashtra', '400056', 99, '9819070494', 19.076000, 72.877701, 0, 1, 0, 40, 0, 0, 0, 1, 0, 'a:5:{s:12:"auto_lat_lng";s:1:"1";s:13:"opening_hours";a:10:{s:12:"opening_type";s:5:"daily";s:10:"daily_days";a:7:{i:0;s:1:"0";i:1;s:1:"1";i:2;s:1:"2";i:3;s:1:"3";i:4;s:1:"4";i:5;s:1:"5";i:6;s:1:"6";}s:11:"daily_hours";a:2:{s:4:"open";s:7:"7:00 PM";s:5:"close";s:7:"1:00 AM";}s:14:"flexible_hours";a:7:{i:0;a:4:{s:3:"day";s:1:"0";s:4:"open";s:8:"12:00 AM";s:5:"close";s:8:"11:59 PM";s:6:"status";s:1:"1";}i:1;a:4:{s:3:"day";s:1:"1";s:4:"open";s:8:"12:00 AM";s:5:"close";s:8:"11:59 PM";s:6:"status";s:1:"1";}i:2;a:4:{s:3:"day";s:1:"2";s:4:"open";s:8:"12:00 AM";s:5:"close";s:8:"11:59 PM";s:6:"status";s:1:"1";}i:3;a:4:{s:3:"day";s:1:"3";s:4:"open";s:8:"12:00 AM";s:5:"close";s:8:"11:59 PM";s:6:"status";s:1:"1";}i:4;a:4:{s:3:"day";s:1:"4";s:4:"open";s:8:"12:00 AM";s:5:"close";s:8:"11:59 PM";s:6:"status";s:1:"1";}i:5;a:4:{s:3:"day";s:1:"5";s:4:"open";s:8:"12:00 AM";s:5:"close";s:8:"11:59 PM";s:6:"status";s:1:"1";}i:6;a:4:{s:3:"day";s:1:"6";s:4:"open";s:8:"12:00 AM";s:5:"close";s:8:"11:59 PM";s:6:"status";s:1:"1";}}s:13:"delivery_type";s:1:"0";s:13:"delivery_days";a:7:{i:0;s:1:"0";i:1;s:1:"1";i:2;s:1:"2";i:3;s:1:"3";i:4;s:1:"4";i:5;s:1:"5";i:6;s:1:"6";}s:14:"delivery_hours";a:2:{s:4:"open";s:8:"12:00 AM";s:5:"close";s:8:"11:59 PM";}s:15:"collection_type";s:1:"0";s:15:"collection_days";a:7:{i:0;s:1:"0";i:1;s:1:"1";i:2;s:1:"2";i:3;s:1:"3";i:4;s:1:"4";i:5;s:1:"5";i:6;s:1:"6";}s:16:"collection_hours";a:2:{s:4:"open";s:8:"12:00 AM";s:5:"close";s:8:"11:59 PM";}}s:13:"future_orders";s:1:"0";s:17:"future_order_days";a:2:{s:8:"delivery";s:1:"5";s:10:"collection";s:1:"5";}s:7:"gallery";a:2:{s:5:"title";s:0:"";s:11:"description";s:0:"";}}', 'data/original-201202-HD-asian-style-pork-burgers.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_location_tables`
--

CREATE TABLE IF NOT EXISTS `hangry_location_tables` (
  `location_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  PRIMARY KEY (`location_id`,`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_mail_templates`
--

CREATE TABLE IF NOT EXISTS `hangry_mail_templates` (
  `template_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `language_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `hangry_mail_templates`
--

INSERT INTO `hangry_mail_templates` (`template_id`, `name`, `language_id`, `date_added`, `date_updated`, `status`) VALUES
(11, 'Default', 1, '2014-04-16 01:49:52', '2014-06-16 14:44:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_mail_templates_data`
--

CREATE TABLE IF NOT EXISTS `hangry_mail_templates_data` (
  `template_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `template_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `subject` varchar(128) NOT NULL,
  `body` text NOT NULL,
  `date_added` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  PRIMARY KEY (`template_data_id`,`template_id`,`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `hangry_mail_templates_data`
--

INSERT INTO `hangry_mail_templates_data` (`template_data_id`, `template_id`, `code`, `subject`, `body`, `date_added`, `date_updated`) VALUES
(11, 11, 'registration', 'Welcome to {site_name}', '<div id="mailsub" class="notification" align="center"><table style="min-width: 320px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" bgcolor="#eff3f8"><!--[if gte mso 10]><table width="680" border="0" cellspacing="0" cellpadding="0"><tr><td><![endif]--><table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr><!--header --><tr><td align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;"><table class="mob_center" style="border-collapse: collapse;" align="left" border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td align="left" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td class="mob_center" align="left" valign="top"><a href="{site_url}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;"><font face="Arial, Helvetica, sans-seri; font-size: 13px;" color="#596167" size="3"><img src="{site_logo}" alt="{site_name}" style="display: block;" height="19" border="0" width="115"></font></a></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--><!--[if gte mso 10]></td><td align="right"><![endif]--><!-- Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;"><table style="border-collapse: collapse;" align="right" border="0" cellpadding="0" cellspacing="0" width="88"><tbody><tr><td align="right" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="right"><!--social --><div class="mob_center_bl" style="width: 88px;"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="line-height: 19px;" align="center" width="30"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="center" width="39"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="right" width="29"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td></tr></tbody></table></div><!--social END--></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--></td></tr></tbody></table><!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"></div></td></tr><!--header END--><!--content 1 --><tr><td align="center" bgcolor="#fbfcfd"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div><div style="line-height: 44px;"><font style="font-size: 34px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="5"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">Welcome!</span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Hello {first_name} {last_name},</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Thank you for registrating with {site_name}. Your account has now been created and you can log in using your email address and password by visiting our website or at the following URL: <a href="{account_login_link}">Click Here</a></span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Thank you for using.<br> {signature}</span></font></div><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div></td></tr></tbody></table></td></tr><!--content 1 END--><!--footer --><tr><td class="iage_footer" align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><font style="font-size: 13px;" face="Arial, Helvetica, sans-serif" color="#96a5b5" size="3"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">2015 © {site_name} All Rights Reserved.</span></font></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><!--footer END--><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr></tbody></table><!--[if gte mso 10]></td></tr></table><![endif]--></td></tr></tbody></table></div>', '2014-04-16 00:56:00', '2014-05-15 15:24:56'),
(12, 11, 'password_reset', 'Password reset at {site_name}', '<div id="mailsub" class="notification" align="center"><table style="min-width: 320px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" bgcolor="#eff3f8"><!--[if gte mso 10]><table width="680" border="0" cellspacing="0" cellpadding="0"><tr><td><![endif]--><table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr><!--header --><tr><td align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;"><table class="mob_center" style="border-collapse: collapse;" align="left" border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td align="left" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td class="mob_center" align="left" valign="top"><a href="{site_url}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;"><font face="Arial, Helvetica, sans-seri; font-size: 13px;" color="#596167" size="3"><img src="{site_logo}" alt="{site_name}" style="display: block;" height="19" border="0" width="115"></font></a></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--><!--[if gte mso 10]></td><td align="right"><![endif]--><!-- Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;"><table style="border-collapse: collapse;" align="right" border="0" cellpadding="0" cellspacing="0" width="88"><tbody><tr><td align="right" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="right"><!--social --><div class="mob_center_bl" style="width: 88px;"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="line-height: 19px;" align="center" width="30"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="center" width="39"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="right" width="29"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td></tr></tbody></table></div><!--social END--></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--></td></tr></tbody></table><!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"></div></td></tr><!--header END--><!--content 1 --><tr><td align="center" bgcolor="#fbfcfd"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div><div style="line-height: 44px;"><font style="font-size: 34px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="5"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">Reset your password!</span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Hello {first_name} {last_name},</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Your password has been reset successfull! Please <a href="{account_login_link}" target="_blank">login</a> using your new password: {created_password}.</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Please don''t forget to change your password after you login.<br> {signature}</span></font></div><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div></td></tr></tbody></table></td></tr><!--content 1 END--><!--footer --><tr><td class="iage_footer" align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><font style="font-size: 13px;" face="Arial, Helvetica, sans-serif" color="#96a5b5" size="3"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">2015 © {site_name} All Rights Reserved.</span></font></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><!--footer END--><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr></tbody></table><!--[if gte mso 10]></td></tr></table><![endif]--></td></tr></tbody></table></div>', '2014-04-16 00:56:00', '2014-05-15 15:46:30'),
(13, 11, 'order', '{site_name} order confirmation - {order_number}', '<div id="mailsub" class="notification" align="center"><table style="min-width: 320px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" bgcolor="#eff3f8"><!--[if gte mso 10]><table width="680" border="0" cellspacing="0" cellpadding="0"><tr><td><![endif]--><table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr><!--header --><tr><td align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;"><table class="mob_center" style="border-collapse: collapse;" align="left" border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td align="left" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td class="mob_center" align="left" valign="top"><a title="" data-original-title="" href="{site_url}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;"><font color="#596167" size="3" face="Arial, Helvetica, sans-seri; font-size: 13px;"><img src="{site_logo}" alt="{site_name}" style="display: block;" border="0" width="115" height="19"></font></a></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--><!--[if gte mso 10]></td><td align="right"><![endif]--><!-- Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;"><table style="border-collapse: collapse;" align="right" border="0" cellpadding="0" cellspacing="0" width="88"><tbody><tr><td align="right" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="right"><!--social --><div class="mob_center_bl" style="width: 88px;"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="line-height: 19px;" align="center" width="30"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font color="#596167" size="2" face="Arial, Helvetica, sans-serif"></font></a></td><td style="line-height: 19px;" align="center" width="39"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font color="#596167" size="2" face="Arial, Helvetica, sans-serif"></font></a></td><td style="line-height: 19px;" align="right" width="29"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font color="#596167" size="2" face="Arial, Helvetica, sans-serif"></font></a></td></tr></tbody></table></div><!--social END--></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--></td></tr></tbody></table><!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"></div></td></tr><!--header END--><!--content 1 --><tr><td align="center" bgcolor="#fbfcfd"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div><div style="line-height: 44px;"><font style="font-size: 34px;" color="#57697e" size="5" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">Thank you for your order!</span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Hello {first_name} {last_name},</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Your order has been received and will be with you shortly. <a title="" data-original-title="" href="{order_view_url}">Click here</a> to view your order progress.</span></font><br></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Your order number is {order_number}<br> This is a {order_type} order.</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><br><strong>Order date:</strong> {order_date}<br><strong>Requested {order_type} time</strong> {order_time}<br><strong>Payment Method:</strong> {order_payment}</span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td><div style="line-height: 24px;"><font style="font-size: 13px;" color="#57697e" size="3" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">Name/Description</span></font></div></td><td><div style="line-height: 24px;"><font style="font-size: 13px;" color="#57697e" size="3" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">Unit Price</span></font></div></td><td><div style="line-height: 24px;"><font style="font-size: 13px;" color="#57697e" size="3" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">Sub Total</span></font></div></td></tr><tr><td>{order_menus}<br></td><td><br></td><td><br></td></tr><tr style="border-top:1px dashed #c3cbd5;"><td><div style="line-height: 24px;"><font style="font-size: 15px;font-weight:bold;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{menu_quantity} x {menu_name}</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" color="#96a5b5" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #96a5b5;">{menu_options}</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" color="#96a5b5" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #96a5b5;">{menu_comment}</span></font></div></td><td><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{menu_price}</span></font></div></td><td><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{menu_subtotal}</span></font></div></td></tr><tr><td>{/order_menus}</td><td><br></td><td><br></td></tr><tr><td><br></td><td>{order_totals}</td><td><br></td></tr><tr><td><br></td><td><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{order_total_title}</span></font></div></td><td><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{order_total_value}</span></font></div></td></tr><tr><td><br></td><td>{/order_totals}<br></td><td><br></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{order_comment}</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{order_address}</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><span title="" data-original-title="" style="font-weight: bold;">Restaurant:</span> {location_name}</span></font></div><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">We hope to see you again soon.<br>{signature}</span></font></div><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div></td></tr></tbody></table></td></tr><!--content 1 END--><!--footer --><tr><td class="iage_footer" align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><font style="font-size: 13px;" color="#96a5b5" size="3" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">2015 © {site_name} All Rights Reserved.</span></font></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><!--footer END--><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr></tbody></table><!--[if gte mso 10]></td></tr></table><![endif]--></td></tr></tbody></table></div>', '2014-04-16 00:56:00', '2014-07-20 14:29:55'),
(14, 11, 'reservation', 'Your Reservation Confirmation - {reservation_number}', '<div id="mailsub" class="notification" align="center"><table style="min-width: 320px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" bgcolor="#eff3f8"><!--[if gte mso 10]><table width="680" border="0" cellspacing="0" cellpadding="0"><tr><td><![endif]--><table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr><!--header --><tr><td align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;"><table class="mob_center" style="border-collapse: collapse;" align="left" border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td align="left" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td class="mob_center" align="left" valign="top"><a href="{site_url}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;"><font face="Arial, Helvetica, sans-seri; font-size: 13px;" color="#596167" size="3"><img src="{site_logo}" alt="{site_name}" style="display: block;" height="19" border="0" width="115"></font></a></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--><!--[if gte mso 10]></td><td align="right"><![endif]--><!-- Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;"><table style="border-collapse: collapse;" align="right" border="0" cellpadding="0" cellspacing="0" width="88"><tbody><tr><td align="right" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="right"><!--social --><div class="mob_center_bl" style="width: 88px;"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="line-height: 19px;" align="center" width="30"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="center" width="39"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="right" width="29"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td></tr></tbody></table></div><!--social END--></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--></td></tr></tbody></table><!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"></div></td></tr><!--header END--><!--content 1 --><tr><td align="center" bgcolor="#fbfcfd"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div><div style="line-height: 44px;"><font style="font-size: 34px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="5"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">Thank you for your reservation!</span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Hello {first_name} {last_name},</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Your reservation {reservation_number} at {location_name} has been booked for {reservation_guest_no} person(s) on {reservation_date} at {reservation_time}.</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Thanks for reserving with us online!</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">We hope to see you again soon.<br>{signature}</span></font></div><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div></td></tr></tbody></table></td></tr><!--content 1 END--><!--footer --><tr><td class="iage_footer" align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><font style="font-size: 13px;" face="Arial, Helvetica, sans-serif" color="#96a5b5" size="3"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">2015 © {site_name} All Rights Reserved.</span></font></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><!--footer END--><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr></tbody></table><!--[if gte mso 10]></td></tr></table><![endif]--></td></tr></tbody></table></div>', '2014-04-16 00:56:00', '2014-07-22 20:13:48'),
(15, 11, 'contact', 'Contact on {site_name}', '<div id="mailsub" class="notification" align="center"><table style="min-width: 320px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" bgcolor="#eff3f8"><!--[if gte mso 10]><table width="680" border="0" cellspacing="0" cellpadding="0"><tr><td><![endif]--><table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr><!--header --><tr><td align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;"><table class="mob_center" style="border-collapse: collapse;" align="left" border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td align="left" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td class="mob_center" align="left" valign="top"><a href="{site_url}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;"><font face="Arial, Helvetica, sans-seri; font-size: 13px;" color="#596167" size="3"><img src="{site_logo}" alt="{site_name}" style="display: block;" height="19" border="0" width="115"></font></a></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--><!--[if gte mso 10]></td><td align="right"><![endif]--><!-- Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;"><table style="border-collapse: collapse;" align="right" border="0" cellpadding="0" cellspacing="0" width="88"><tbody><tr><td align="right" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="right"><!--social --><div class="mob_center_bl" style="width: 88px;"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="line-height: 19px;" align="center" width="30"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="center" width="39"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="right" width="29"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td></tr></tbody></table></div><!--social END--></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--></td></tr></tbody></table><!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"></div></td></tr><!--header END--><!--content 1 --><tr><td align="center" bgcolor="#fbfcfd"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div><div style="line-height: 44px;"><font style="font-size: 34px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="5"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">Someone just contacted you!</span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Hello Admin,</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"><br></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">From: {full_name}</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Topic: {contact_topic}.</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Telephone: {contact_telephone}.</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><br></span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{contact_message}</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><br></span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">This inquiry was sent from {site_name}</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{signature}<br></span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr></tbody></table></td></tr><!--content 1 END--><!--footer --><tr><td class="iage_footer" align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><font style="font-size: 13px;" face="Arial, Helvetica, sans-serif" color="#96a5b5" size="3"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">2015 © {site_name} All Rights Reserved.</span></font></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><!--footer END--><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr></tbody></table><!--[if gte mso 10]></td></tr></table><![endif]--></td></tr></tbody></table></div>', '2014-04-16 00:56:00', '2014-05-15 18:00:57'),
(16, 11, 'internal', 'Subject here', 'Body here', '2014-04-16 00:56:00', '2014-04-16 00:59:00'),
(17, 11, 'order_alert', 'New order on {site_name}', '<div id="mailsub" class="notification" align="center"><table style="min-width: 320px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" bgcolor="#eff3f8"><!--[if gte mso 10]><table width="680" border="0" cellspacing="0" cellpadding="0"><tr><td><![endif]--><table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr><!--header --><tr><td align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;"><table class="mob_center" style="border-collapse: collapse;" align="left" border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td align="left" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td class="mob_center" align="left" valign="top"><a title="" data-original-title="" href="{site_url}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;"><font color="#596167" size="3" face="Arial, Helvetica, sans-seri; font-size: 13px;"><img src="{site_logo}" alt="{site_name}" style="display: block;" border="0" width="115" height="19"></font></a></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--><!--[if gte mso 10]></td><td align="right"><![endif]--><!-- Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;"><table style="border-collapse: collapse;" align="right" border="0" cellpadding="0" cellspacing="0" width="88"><tbody><tr><td align="right" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="right"><!--social --><div class="mob_center_bl" style="width: 88px;"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="line-height: 19px;" align="center" width="30"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font color="#596167" size="2" face="Arial, Helvetica, sans-serif"></font></a></td><td style="line-height: 19px;" align="center" width="39"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font color="#596167" size="2" face="Arial, Helvetica, sans-serif"></font></a></td><td style="line-height: 19px;" align="right" width="29"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font color="#596167" size="2" face="Arial, Helvetica, sans-serif"></font></a></td></tr></tbody></table></div><!--social END--></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--></td></tr></tbody></table><!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"></div></td></tr><!--header END--><!--content 1 --><tr><td align="center" bgcolor="#fbfcfd"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div><div style="line-height: 44px;"><font style="font-size: 34px;" color="#57697e" size="5" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">You received an order!</span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">You just received an order from {location_name}.</span></font><br></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">The order number is {order_number}<br> This is a {order_type} order.</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><br><strong>Customer name:</strong> {first_name} {last_name}<br><strong>Order date:</strong> {order_date}<br><strong>Requested {order_type} time</strong> {order_time}<br><strong>Payment Method:</strong> {order_payment}<br><br></span></font></div><!-- padding --><div style="height: 10px; line-height: 10px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"></span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td><div style="line-height: 24px;"><font style="font-size: 13px;" color="#57697e" size="3" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">Name/Description</span></font></div></td><td><div style="line-height: 24px;"><font style="font-size: 13px;" color="#57697e" size="3" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">Unit Price</span></font></div></td><td><div style="line-height: 24px;"><font style="font-size: 13px;" color="#57697e" size="3" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">Sub Total</span></font></div></td></tr><tr><td>{order_menus}<br></td><td><br></td><td><br></td></tr><tr style="border-top:1px dashed #c3cbd5;"><td><div style="line-height: 24px;"><font style="font-size: 15px;font-weight:bold;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{menu_quantity} x {menu_name}</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" color="#96a5b5" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #96a5b5;">{menu_options}</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" color="#96a5b5" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #96a5b5;">{menu_comment}</span></font></div></td><td><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{menu_price}</span></font></div></td><td><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{menu_subtotal}</span></font></div></td></tr><tr><td>{/order_menus}</td><td><br></td><td><br></td></tr><tr><td><br></td><td>{order_totals}</td><td><br></td></tr><tr><td><br></td><td><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{order_total_title}</span></font></div></td><td><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{order_total_value}</span></font></div></td></tr><tr><td><br></td><td>{/order_totals}<br></td><td><br></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" color="#57697e" size="4" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{order_comment}</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div></td></tr></tbody></table></td></tr><!--content 1 END--><!--footer --><tr><td class="iage_footer" align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><font style="font-size: 13px;" color="#96a5b5" size="3" face="Arial, Helvetica, sans-serif"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">2015 © {site_name} All Rights Reserved.</span></font></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><!--footer END--><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr></tbody></table><!--[if gte mso 10]></td></tr></table><![endif]--></td></tr></tbody></table></div>', '2014-04-16 00:56:00', '2014-04-16 00:59:00');
INSERT INTO `hangry_mail_templates_data` (`template_data_id`, `template_id`, `code`, `subject`, `body`, `date_added`, `date_updated`) VALUES
(18, 11, 'reservation_alert', 'New reservation on {site_name}', '<div id="mailsub" class="notification" align="center"><table style="min-width: 320px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" bgcolor="#eff3f8"><!--[if gte mso 10]><table width="680" border="0" cellspacing="0" cellpadding="0"><tr><td><![endif]--><table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr><!--header --><tr><td align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;"><table class="mob_center" style="border-collapse: collapse;" align="left" border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td align="left" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td class="mob_center" align="left" valign="top"><a href="{site_url}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;"><font face="Arial, Helvetica, sans-seri; font-size: 13px;" color="#596167" size="3"><img src="{site_logo}" alt="{site_name}" style="display: block;" height="19" border="0" width="115"></font></a></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--><!--[if gte mso 10]></td><td align="right"><![endif]--><!-- Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;"><table style="border-collapse: collapse;" align="right" border="0" cellpadding="0" cellspacing="0" width="88"><tbody><tr><td align="right" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="right"><!--social --><div class="mob_center_bl" style="width: 88px;"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="line-height: 19px;" align="center" width="30"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="center" width="39"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="right" width="29"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td></tr></tbody></table></div><!--social END--></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--></td></tr></tbody></table><!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"></div></td></tr><!--header END--><!--content 1 --><tr><td align="center" bgcolor="#fbfcfd"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div><div style="line-height: 44px;"><font style="font-size: 34px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="5"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">You received a table reservation!</span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><span style="font-weight: bold;">Customer name:</span> {first_name} {last_name}</span></font></div><!-- padding --></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><span style="font-weight: bold;">Reservation no:</span> {reservation_number} </span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><span style="font-weight: bold;">Restaurant:</span> {location_name} </span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><span style="font-weight: bold;">No of guest(s):</span> {reservation_guest_no} person(s) </span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><span style="font-weight: bold;">Reservation date:</span> {reservation_date}</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><span style="font-weight: bold;">Reservation time: </span></span></font>{reservation_time}</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">You received a table reservation from {site_name}<br></span></font></div><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div></td></tr></tbody></table></td></tr><!--content 1 END--><!--footer --><tr><td class="iage_footer" align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><font style="font-size: 13px;" face="Arial, Helvetica, sans-serif" color="#96a5b5" size="3"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">2015 © {site_name} All Rights Reserved.</span></font></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><!--footer END--><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr></tbody></table><!--[if gte mso 10]></td></tr></table><![endif]--></td></tr></tbody></table></div>', '2014-04-16 00:56:00', '2014-04-16 00:59:00'),
(19, 11, 'registration_alert', 'New Customer on {site_name}', '<div id="mailsub" class="notification" align="center"><table style="min-width: 320px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" bgcolor="#eff3f8"><!--[if gte mso 10]><table width="680" border="0" cellspacing="0" cellpadding="0"><tr><td><![endif]--><table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr><!--header --><tr><td align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;"><table class="mob_center" style="border-collapse: collapse;" align="left" border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td align="left" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td class="mob_center" align="left" valign="top"><a href="{site_url}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;"><font face="Arial, Helvetica, sans-seri; font-size: 13px;" color="#596167" size="3"><img src="{site_logo}" alt="{site_name}" style="display: block;" height="19" border="0" width="115"></font></a></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--><!--[if gte mso 10]></td><td align="right"><![endif]--><!-- Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;"><table style="border-collapse: collapse;" align="right" border="0" cellpadding="0" cellspacing="0" width="88"><tbody><tr><td align="right" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="right"><!--social --><div class="mob_center_bl" style="width: 88px;"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="line-height: 19px;" align="center" width="30"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="center" width="39"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="right" width="29"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td></tr></tbody></table></div><!--social END--></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--></td></tr></tbody></table><!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"></div></td></tr><!--header END--><!--content 1 --><tr><td align="center" bgcolor="#fbfcfd"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div><div style="line-height: 44px;"><font style="font-size: 34px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="5"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">You have a new customer!</span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><span style="font-weight: bold;">Customer name:</span> {first_name} {last_name}</span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr></tbody></table></td></tr><!--content 1 END--><!--footer --><tr><td class="iage_footer" align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><font style="font-size: 13px;" face="Arial, Helvetica, sans-serif" color="#96a5b5" size="3"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">2015 © {site_name} All Rights Reserved.</span></font></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><!--footer END--><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr></tbody></table><!--[if gte mso 10]></td></tr></table><![endif]--></td></tr></tbody></table></div>', '2016-08-28 00:00:00', '2016-08-28 00:00:00'),
(20, 11, 'password_reset_alert', 'Password reset at {site_name}', '<div id="mailsub" class="notification" align="center"><table style="min-width: 320px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" bgcolor="#eff3f8"><!--[if gte mso 10]><table width="680" border="0" cellspacing="0" cellpadding="0"><tr><td><![endif]--><table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr><!--header --><tr><td align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;"><table class="mob_center" style="border-collapse: collapse;" align="left" border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td align="left" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td class="mob_center" align="left" valign="top"><a href="{site_url}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;"><font face="Arial, Helvetica, sans-seri; font-size: 13px;" color="#596167" size="3"><img src="{site_logo}" alt="{site_name}" style="display: block;" height="19" border="0" width="115"></font></a></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--><!--[if gte mso 10]></td><td align="right"><![endif]--><!-- Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;"><table style="border-collapse: collapse;" align="right" border="0" cellpadding="0" cellspacing="0" width="88"><tbody><tr><td align="right" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="right"><!--social --><div class="mob_center_bl" style="width: 88px;"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="line-height: 19px;" align="center" width="30"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="center" width="39"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="right" width="29"><a href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td></tr></tbody></table></div><!--social END--></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--></td></tr></tbody></table><!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"></div></td></tr><!--header END--><!--content 1 --><tr><td align="center" bgcolor="#fbfcfd"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div><div style="line-height: 44px;"><font style="font-size: 34px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="5"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 34px; color: #57697e;">Reset your password!</span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Hello {staff_name},</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">You requested that the password be reset for the following account:</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Username: {staff_username}</span></font></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Password: {created_password}</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Please do not forget to change your password after you login.<br> {signature}</span></font></div><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div></td></tr></tbody></table></td></tr><!--content 1 END--><!--footer --><tr><td class="iage_footer" align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><font style="font-size: 13px;" face="Arial, Helvetica, sans-serif" color="#96a5b5" size="3"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">2015 © {site_name} All Rights Reserved.</span></font></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><!--footer END--><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr></tbody></table><!--[if gte mso 10]></td></tr></table><![endif]--></td></tr></tbody></table></div>', '2016-08-28 00:00:00', '2016-08-28 00:00:00'),
(21, 11, 'order_update', 'Your Order Update - {order_number}', '<div id="mailsub" class="notification" align="center"><table style="min-width: 320px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" bgcolor="#eff3f8"><!--[if gte mso 10]><table width="680" border="0" cellspacing="0" cellpadding="0"><tr><td><![endif]--><table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr><!--header --><tr><td align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;"><table class="mob_center" style="border-collapse: collapse;" align="left" border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td align="left" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td class="mob_center" align="left" valign="top"><a title="" data-original-title="" href="{site_url}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;"><font face="Arial, Helvetica, sans-seri; font-size: 13px;" color="#596167" size="3"><img src="{site_logo}" alt="{site_name}" style="display: block;" height="19" border="0" width="115"></font></a></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--><!--[if gte mso 10]></td><td align="right"><![endif]--><!-- Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;"><table style="border-collapse: collapse;" align="right" border="0" cellpadding="0" cellspacing="0" width="88"><tbody><tr><td align="right" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="right"><!--social --><div class="mob_center_bl" style="width: 88px;"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="line-height: 19px;" align="center" width="30"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="center" width="39"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="right" width="29"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td></tr></tbody></table></div><!--social END--></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--></td></tr></tbody></table><!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"></div></td></tr><!--header END--><!--content 1 --><tr><td align="center" bgcolor="#fbfcfd"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Your order has been updated to the following status: <span title="" data-original-title="" style="font-weight: bold;">{status_name}</span></span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;"><a title="" data-original-title="" href="{order_view_url}">Click here</a> to view your order progress.</span></font><br></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Your order number is: <span title="" data-original-title="" style="font-weight: bold;">{order_number}</span></span></font></div><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><span title="" data-original-title="" style="font-weight: bold;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">The comments for your order are:</span></font></span></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{status_comment}</span></font></div><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">We hope to see you again soon.<br>{signature}</span></font></div><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div></td></tr></tbody></table></td></tr><!--content 1 END--><!--footer --><tr><td class="iage_footer" align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><font style="font-size: 13px;" face="Arial, Helvetica, sans-serif" color="#96a5b5" size="3"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">2015 © {site_name} All Rights Reserved.</span></font></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><!--footer END--><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr></tbody></table><!--[if gte mso 10]></td></tr></table><![endif]--></td></tr></tbody></table></div>', '2016-08-28 00:00:00', '2016-08-28 00:00:00'),
(22, 11, 'reservation_update', 'Your Reservation Update - {reservation_number}', '<div id="mailsub" class="notification" align="center"><table style="min-width: 320px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center" bgcolor="#eff3f8"><!--[if gte mso 10]><table width="680" border="0" cellspacing="0" cellpadding="0"><tr><td><![endif]--><table class="table_width_100" style="max-width: 680px; min-width: 300px;" border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr><!--header --><tr><td align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 30px; line-height: 30px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><!-- Item --><div class="mob_center_bl" style="float: left; display: inline-block; width: 115px;"><table class="mob_center" style="border-collapse: collapse;" align="left" border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td align="left" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="115"><tbody><tr><td class="mob_center" align="left" valign="top"><a title="" data-original-title="" href="{site_url}" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 13px;"><font face="Arial, Helvetica, sans-seri; font-size: 13px;" color="#596167" size="3"><img src="{site_logo}" alt="{site_name}" style="display: block;" height="19" border="0" width="115"></font></a></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--><!--[if gte mso 10]></td><td align="right"><![endif]--><!-- Item --><div class="mob_center_bl" style="float: right; display: inline-block; width: 88px;"><table style="border-collapse: collapse;" align="right" border="0" cellpadding="0" cellspacing="0" width="88"><tbody><tr><td align="right" valign="middle"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="right"><!--social --><div class="mob_center_bl" style="width: 88px;"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td style="line-height: 19px;" align="center" width="30"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="center" width="39"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td><td style="line-height: 19px;" align="right" width="29"><a title="" data-original-title="" href="#" target="_blank" style="color: #596167; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><font face="Arial, Helvetica, sans-serif" color="#596167" size="2"></font></a></td></tr></tbody></table></div><!--social END--></td></tr></tbody></table></td></tr></tbody></table></div><!-- Item END--></td></tr></tbody></table><!-- padding --><div style="height: 50px; line-height: 50px; font-size: 10px;"></div></td></tr><!--header END--><!--content 1 --><tr><td align="center" bgcolor="#fbfcfd"><table border="0" cellpadding="0" cellspacing="0" width="90%"><tbody><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Your reservation has been updated to the following status: <span title="" data-original-title="" style="font-weight: bold;">{status_name}</span></span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">Your reservation number: <span title="" data-original-title="" style="font-weight: bold;">{reservation_number}</span> at <span title="" data-original-title="" style="font-weight: bold;">{location_name}</span>.</span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><span title="" data-original-title="" style="font-weight: bold;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">The comments for your reservation are:</span></font></span></div><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">{status_comment}<br></span></font></div><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><tr><td align="left"><div style="line-height: 24px;"><font style="font-size: 15px;" face="Arial, Helvetica, sans-serif" color="#57697e" size="4"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; color: #57697e;">We hope to see you again soon.<br>{signature}</span></font></div><!-- padding --><div style="height: 40px; line-height: 40px; font-size: 10px;"></div></td></tr></tbody></table></td></tr><!--content 1 END--><!--footer --><tr><td class="iage_footer" align="center" bgcolor="#ffffff"><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div><table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody><tr><td align="center"><font style="font-size: 13px;" face="Arial, Helvetica, sans-serif" color="#96a5b5" size="3"><span title="" data-original-title="" style="font-family: Arial, Helvetica, sans-serif; font-size: 13px; color: #96a5b5;">2015 © {site_name} All Rights Reserved.</span></font></td></tr></tbody></table><!-- padding --><div style="height: 20px; line-height: 20px; font-size: 10px;"></div></td></tr><!--footer END--><tr><td><!-- padding --><div style="height: 80px; line-height: 80px; font-size: 10px;"></div></td></tr></tbody></table><!--[if gte mso 10]></td></tr></table><![endif]--></td></tr></tbody></table></div>', '2016-08-28 00:00:00', '2016-08-28 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_mealtimes`
--

CREATE TABLE IF NOT EXISTS `hangry_mealtimes` (
  `mealtime_id` int(11) NOT NULL AUTO_INCREMENT,
  `mealtime_name` varchar(128) NOT NULL,
  `start_time` time NOT NULL DEFAULT '00:00:00',
  `end_time` time NOT NULL DEFAULT '23:59:59',
  `mealtime_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`mealtime_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `hangry_mealtimes`
--

INSERT INTO `hangry_mealtimes` (`mealtime_id`, `mealtime_name`, `start_time`, `end_time`, `mealtime_status`) VALUES
(11, 'Breakfast', '07:00:00', '10:00:00', 0),
(12, 'Lunch', '12:00:00', '14:30:00', 0),
(13, 'Dinner', '18:00:00', '20:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_menus`
--

CREATE TABLE IF NOT EXISTS `hangry_menus` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) NOT NULL,
  `menu_description` text NOT NULL,
  `menu_price` decimal(15,4) NOT NULL,
  `menu_photo` varchar(255) NOT NULL,
  `menu_category_id` int(11) NOT NULL,
  `stock_qty` int(11) NOT NULL,
  `minimum_qty` int(11) NOT NULL,
  `subtract_stock` tinyint(1) NOT NULL,
  `mealtime_id` int(11) NOT NULL,
  `menu_status` tinyint(1) NOT NULL,
  `menu_priority` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `hangry_menus`
--

INSERT INTO `hangry_menus` (`menu_id`, `menu_name`, `menu_description`, `menu_price`, `menu_photo`, `menu_category_id`, `stock_qty`, `minimum_qty`, `subtract_stock`, `mealtime_id`, `menu_status`, `menu_priority`) VALUES
(11, 'Lotus Stem Tango', 'Slice of lotus stem in a tangy, tasty medley. Served with sweet, sour, spicy honey & chilli sauce', 295.0000, '', 11, 0, 1, 0, 13, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_menus_specials`
--

CREATE TABLE IF NOT EXISTS `hangry_menus_specials` (
  `special_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `special_price` decimal(15,4) DEFAULT NULL,
  `special_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`special_id`,`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_menu_options`
--

CREATE TABLE IF NOT EXISTS `hangry_menu_options` (
  `menu_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `required` tinyint(4) NOT NULL,
  `default_value_id` tinyint(4) NOT NULL,
  `option_values` text NOT NULL,
  PRIMARY KEY (`menu_option_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_menu_option_values`
--

CREATE TABLE IF NOT EXISTS `hangry_menu_option_values` (
  `menu_option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_option_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `new_price` decimal(15,4) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `subtract_stock` tinyint(4) NOT NULL,
  PRIMARY KEY (`menu_option_value_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_messages`
--

CREATE TABLE IF NOT EXISTS `hangry_messages` (
  `message_id` int(15) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `send_type` varchar(32) NOT NULL,
  `recipient` varchar(32) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_message_meta`
--

CREATE TABLE IF NOT EXISTS `hangry_message_meta` (
  `message_meta_id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `item` varchar(32) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`message_meta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_migrations`
--

CREATE TABLE IF NOT EXISTS `hangry_migrations` (
  `type` varchar(40) DEFAULT NULL,
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hangry_migrations`
--

INSERT INTO `hangry_migrations` (`type`, `version`) VALUES
('core', 30),
('cart_module', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_options`
--

CREATE TABLE IF NOT EXISTS `hangry_options` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_name` varchar(32) NOT NULL,
  `display_type` varchar(15) NOT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_option_values`
--

CREATE TABLE IF NOT EXISTS `hangry_option_values` (
  `option_value_id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `value` varchar(128) NOT NULL,
  `price` decimal(15,4) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  PRIMARY KEY (`option_value_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_orders`
--

CREATE TABLE IF NOT EXISTS `hangry_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(32) NOT NULL,
  `location_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `cart` text NOT NULL,
  `total_items` int(11) NOT NULL,
  `comment` text NOT NULL,
  `payment` varchar(35) NOT NULL,
  `order_type` varchar(32) NOT NULL,
  `date_added` datetime NOT NULL,
  `date_modified` date NOT NULL,
  `order_time` time NOT NULL,
  `order_date` date NOT NULL,
  `order_total` decimal(15,4) DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `assignee_id` int(11) NOT NULL,
  `invoice_no` int(11) NOT NULL,
  `invoice_prefix` varchar(32) NOT NULL,
  `invoice_date` datetime NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20001 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_order_menus`
--

CREATE TABLE IF NOT EXISTS `hangry_order_menus` (
  `order_menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(15,4) DEFAULT NULL,
  `subtotal` decimal(15,4) DEFAULT NULL,
  `option_values` text NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`order_menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_order_options`
--

CREATE TABLE IF NOT EXISTS `hangry_order_options` (
  `order_option_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `order_option_name` varchar(128) NOT NULL,
  `order_option_price` decimal(15,4) DEFAULT NULL,
  `order_menu_id` int(11) NOT NULL,
  `order_menu_option_id` int(11) NOT NULL,
  `menu_option_value_id` int(11) NOT NULL,
  PRIMARY KEY (`order_option_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_order_totals`
--

CREATE TABLE IF NOT EXISTS `hangry_order_totals` (
  `order_total_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `code` varchar(30) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` decimal(15,2) NOT NULL,
  `priority` tinyint(1) NOT NULL,
  PRIMARY KEY (`order_total_id`,`order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_pages`
--

CREATE TABLE IF NOT EXISTS `hangry_pages` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `title` varchar(255) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `layout_id` int(11) NOT NULL,
  `navigation` text NOT NULL,
  `date_added` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `hangry_pages`
--

INSERT INTO `hangry_pages` (`page_id`, `language_id`, `name`, `title`, `heading`, `content`, `meta_description`, `meta_keywords`, `layout_id`, `navigation`, `date_added`, `date_updated`, `status`) VALUES
(11, 11, 'About Us', 'About Us', 'About Us', '<h3 style="text-align: center;"><span style="color: #993300;">Aim</span></h3>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In venenatis massa ac magna sagittis, sit amet gravida metus gravida. Aenean dictum pellentesque erat, vitae adipiscing libero semper sit amet. Vestibulum nec nunc lorem. Duis vitae libero a libero hendrerit tincidunt in eu tellus. Aliquam consequat ultrices felis ut dictum. Nulla euismod felis a sem mattis ornare. Aliquam ut diam sit amet dolor iaculis molestie ac id nisl. Maecenas hendrerit convallis mi feugiat gravida. Quisque tincidunt, leo a posuere imperdiet, metus leo vestibulum orci, vel volutpat justo ligula id quam. Cras placerat tincidunt lorem eu interdum.</p>\r\n<h3 style="text-align: center;"><span style="color: #993300;">Mission</span></h3>\r\n<p>Ut eu pretium urna. In sed consectetur neque. In ornare odio erat, id ornare arcu euismod a. Ut dapibus sit amet erat commodo vestibulum. Praesent vitae lacus faucibus, rhoncus tortor et, bibendum justo. Etiam pharetra congue orci, eget aliquam orci. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eleifend justo eros, sit amet fermentum tellus ullamcorper quis. Cras cursus mi at imperdiet faucibus. Proin iaculis, felis vitae luctus venenatis, ante tortor porta nisi, et ornare magna metus sit amet enim. Phasellus et turpis nec metus aliquet adipiscing. Etiam at augue nec odio lacinia tincidunt. Suspendisse commodo commodo ipsum ac sollicitudin. Nunc nec consequat lacus. Donec gravida rhoncus justo sed elementum.</p>\r\n<h3 style="text-align: center;"><span style="color: #a52a2a;">Vision</span></h3>\r\n<p>Praesent erat massa, consequat a nulla et, eleifend facilisis risus. Nullam libero mi, bibendum id eleifend vitae, imperdiet a nulla. Fusce congue porta ultricies. Vivamus felis lectus, egestas at pretium vitae, posuere a nibh. Mauris lobortis urna nec rhoncus consectetur. Fusce sed placerat sem. Nulla venenatis elit risus, non auctor arcu lobortis eleifend. Ut aliquet vitae velit a faucibus. Suspendisse quis risus sit amet arcu varius malesuada. Vestibulum vitae massa consequat, euismod lorem a, euismod lacus. Duis sagittis dolor risus, ac vehicula mauris lacinia quis. Nulla facilisi. Duis tristique ipsum nec egestas auctor. Nullam in felis vel ligula dictum tincidunt nec a neque. Praesent in egestas elit.</p>', '', '', 17, 'a:2:{i:0;s:8:"side_bar";i:1;s:6:"footer";}', '2014-04-19 16:57:21', '2015-05-07 12:39:52', 1),
(12, 11, 'Policy', 'Policy', 'Policy', '<div id="lipsum">\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ligula eros, semper a lorem et, venenatis volutpat dolor. Pellentesque hendrerit lectus feugiat nulla cursus, quis dapibus dolor porttitor. Donec velit enim, adipiscing ac orci id, congue tincidunt arcu. Proin egestas nulla eget leo scelerisque, et semper diam ornare. Suspendisse potenti. Suspendisse vitae bibendum enim. Duis eu ligula hendrerit, lacinia felis in, mollis nisi. Sed gravida arcu in laoreet dictum. Nulla faucibus lectus a mollis dapibus. Fusce vehicula convallis urna, et congue nulla ultricies in. Nulla magna velit, bibendum eu odio et, euismod rhoncus sem. Nullam quis magna fermentum, ultricies neque nec, blandit neque. Etiam nec congue arcu. Curabitur sed tellus quam. Cras adipiscing odio odio, et porttitor dui suscipit eget. Aliquam non est commodo, elementum turpis at, pellentesque lorem.</p>\r\n<p>Duis nec diam diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis vulputate est et lorem sagittis, et mollis libero ultricies. Nunc ultrices tortor vel convallis varius. In dolor dolor, scelerisque ac faucibus ut, aliquet ac sem. Praesent consectetur lacus quis tristique posuere. Nulla sed ultricies odio. Cras tristique vulputate facilisis.</p>\r\n<p>Mauris at metus in magna condimentum gravida eu tincidunt urna. Praesent sodales vel mi eu condimentum. Suspendisse in luctus purus. Vestibulum dignissim, metus non luctus accumsan, odio ligula pharetra massa, in eleifend turpis risus in diam. Sed non lorem nibh. Nam at feugiat urna. Curabitur interdum, diam sit amet pulvinar blandit, mauris ante scelerisque nisi, sit amet placerat mi nunc eget orci. Nulla eget quam sit amet risus rhoncus lacinia a ut eros. Praesent non libero nisi. Mauris tincidunt at purus sit amet adipiscing. Donec interdum, velit nec dignissim vehicula, libero ipsum imperdiet ligula, lacinia mattis augue dui ac lacus. Aenean molestie sed nunc at pulvinar. Fusce ornare lacus non venenatis rhoncus.</p>\r\n<p>Aenean at enim luctus ante commodo consequat nec ut mi. Sed porta adipiscing tempus. Aliquam sit amet ullamcorper ipsum, id adipiscing quam. Fusce iaculis odio ut nisi convallis hendrerit. Morbi auctor adipiscing ligula, sit amet aliquet ante consectetur at. Donec vulputate neque eleifend libero pellentesque, vitae lacinia enim ornare. Vestibulum fermentum erat blandit, ultricies felis ac, facilisis augue. Nulla facilisis mi porttitor, interdum diam in, lobortis ipsum. In molestie quam nisl, lacinia convallis tellus fermentum ac. Nulla quis velit augue. Fusce accumsan, lacus et lobortis blandit, neque magna gravida enim, dignissim ultricies tortor dui in dolor. Vestibulum vel convallis justo, quis venenatis elit. Aliquam erat volutpat. Nunc quis iaculis ligula. Suspendisse dictum sodales neque vitae faucibus. Fusce id tellus pretium, varius nunc et, placerat metus.</p>\r\n<p>Pellentesque quis facilisis mauris. Phasellus porta, metus a dignissim viverra, est elit luctus erat, nec ultricies ligula lorem eget sapien. Pellentesque ac justo velit. Maecenas semper accumsan nulla eget rhoncus. Aliquam vel urna sed nibh dignissim auctor. Integer volutpat lacus ac purus convallis, at lobortis nisi tincidunt. Vestibulum condimentum elit ac sapien placerat, at ornare libero hendrerit. Cras tincidunt nunc sit amet ante bibendum tempor. Fusce quam orci, suscipit sed eros quis, vulputate molestie metus. Nam hendrerit vitae felis et porttitor. Proin et commodo velit, id porta erat. Donec eu consectetur odio. Fusce porta odio risus. Aliquam vel erat feugiat, vestibulum elit eget, ornare sapien. Sed sed nulla justo. Sed a dolor eu justo lacinia blandit</p>\r\n</div>', '', '', 17, 'a:2:{i:0;s:8:"side_bar";i:1;s:6:"footer";}', '2014-04-19 17:21:23', '2015-05-16 09:18:39', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_permalinks`
--

CREATE TABLE IF NOT EXISTS `hangry_permalinks` (
  `permalink_id` int(11) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `controller` varchar(255) NOT NULL,
  `query` varchar(255) NOT NULL,
  PRIMARY KEY (`permalink_id`),
  UNIQUE KEY `uniqueSlug` (`slug`,`controller`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `hangry_permalinks`
--

INSERT INTO `hangry_permalinks` (`permalink_id`, `slug`, `controller`, `query`) VALUES
(11, 'about-us', 'pages', 'page_id=11');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_permissions`
--

CREATE TABLE IF NOT EXISTS `hangry_permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` varchar(255) NOT NULL,
  `action` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `hangry_permissions`
--

INSERT INTO `hangry_permissions` (`permission_id`, `name`, `description`, `action`, `status`) VALUES
(11, 'Admin.Banners', 'Ability to access, manage, add and delete banners', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(12, 'Admin.Categories', 'Ability to access, manage, add and delete categories', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(13, 'Site.Countries', 'Ability to manage, add and delete site countries', 'a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}', 1),
(14, 'Admin.Coupons', 'Ability to access, manage, add and delete coupons', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(15, 'Site.Currencies', 'Ability to access, manage, add and delete site currencies', 'a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}', 1),
(16, 'Admin.CustomerGroups', 'Ability to access, manage, add and delete customer groups', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(17, 'Admin.Customers', 'Ability to access, manage, add and delete customers', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(18, 'Admin.CustomersOnline', 'Ability to access online customers', 'a:1:{i:0;s:6:"access";}', 1),
(19, 'Admin.Maintenance', 'Ability to access, backup, restore and migrate database', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(20, 'Admin.ErrorLogs', 'Ability to access and delete error logs file', 'a:2:{i:0;s:6:"access";i:1;s:6:"delete";}', 1),
(21, 'Admin.Extensions', 'Ability to access, manage, add and delete extension', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(22, 'Admin.MediaManager', 'Ability to access, manage, add and delete media items', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(23, 'Site.Languages', 'Ability to manage, add and delete site languages', 'a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}', 1),
(24, 'Site.Layouts', 'Ability to manage, add and delete site layouts', 'a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}', 1),
(25, 'Admin.Locations', 'Ability to access, manage, add and delete locations', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(26, 'Admin.MailTemplates', 'Ability to access, manage, add and delete mail templates', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(27, 'Admin.MenuOptions', 'Ability to access, manage, add and delete menu option items', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(28, 'Admin.Menus', 'Ability to access, manage, add and delete menu items', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(29, 'Admin.Messages', 'Ability to add and delete messages', 'a:2:{i:0;s:3:"add";i:1;s:6:"delete";}', 1),
(30, 'Admin.Orders', 'Ability to access, manage, add and delete orders', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(31, 'Site.Pages', 'Ability to manage, add and delete site pages', 'a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}', 1),
(32, 'Admin.Payments', 'Ability to access, add and delete extension payments', 'a:3:{i:0;s:6:"access";i:1;s:3:"add";i:2;s:6:"delete";}', 1),
(33, 'Admin.Permissions', 'Ability to manage, add and delete staffs permissions', 'a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}', 1),
(34, 'Admin.Ratings', 'Ability to add and delete review ratings', 'a:2:{i:0;s:3:"add";i:1;s:6:"delete";}', 1),
(35, 'Admin.Reservations', 'Ability to access, manage, add and delete reservations', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(36, 'Admin.Reviews', 'Ability to access, manage, add and delete user reviews', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(37, 'Admin.SecurityQuestions', 'Ability to add and delete customer registration security questions', 'a:2:{i:0;s:3:"add";i:1;s:6:"delete";}', 1),
(38, 'Site.Settings', 'Ability to manage system settings', 'a:1:{i:0;s:6:"manage";}', 1),
(39, 'Admin.StaffGroups', 'Ability to access, manage, add and delete staff groups', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(40, 'Admin.Staffs', 'Ability to access, manage, add and delete staffs', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(42, 'Admin.Statuses', 'Ability to access, manage, add and delete orders and reservations statuses', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(43, 'Admin.Tables', 'Ability to access, manage, add and delete reservations tables', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(44, 'Site.Themes', 'Ability to access, manage site themes', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1),
(45, 'Module.AccountModule', 'Ability to manage account module', 'a:1:{i:0;s:6:"manage";}', 1),
(46, 'Module.BannersModule', 'Ability to manage banners module', 'a:1:{i:0;s:6:"manage";}', 1),
(47, 'Module.CartModule', 'Ability to manage cart module', 'a:1:{i:0;s:6:"manage";}', 1),
(48, 'Module.CategoriesModule', 'Ability to manage categories module', 'a:1:{i:0;s:6:"manage";}', 1),
(49, 'Module.LocalModule', 'Ability to manage local module', 'a:1:{i:0;s:6:"manage";}', 1),
(50, 'Module.PagesModule', 'Ability to manage pages module', 'a:1:{i:0;s:6:"manage";}', 1),
(51, 'Module.ReservationModule', 'Ability to manage reservation module', 'a:1:{i:0;s:6:"manage";}', 1),
(52, 'Module.Slideshow', 'Ability to manage slideshow module', 'a:1:{i:0;s:6:"manage";}', 1),
(53, 'Payment.Cod', 'Ability to manage cash on delivery payment', 'a:1:{i:0;s:6:"manage";}', 1),
(54, 'Payment.PaypalExpress', 'Ability to manage paypal express payment', 'a:1:{i:0;s:6:"manage";}', 1),
(55, 'Site.Updates', 'Ability to apply updates when a new version of TastyIgniter is available', 'a:1:{i:0;s:3:"add";}', 1),
(56, 'Admin.Mealtimes', 'Ability to access, manage, add and delete mealtimes', 'a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_pp_payments`
--

CREATE TABLE IF NOT EXISTS `hangry_pp_payments` (
  `transaction_id` varchar(19) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `serialized` text NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_reservations`
--

CREATE TABLE IF NOT EXISTS `hangry_reservations` (
  `reservation_id` int(32) NOT NULL AUTO_INCREMENT,
  `location_id` int(11) NOT NULL,
  `table_id` int(11) NOT NULL,
  `guest_num` int(11) NOT NULL,
  `occasion_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(96) NOT NULL,
  `telephone` varchar(45) NOT NULL,
  `comment` text NOT NULL,
  `reserve_time` time NOT NULL,
  `reserve_date` date NOT NULL,
  `date_added` date NOT NULL,
  `date_modified` date NOT NULL,
  `assignee_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`reservation_id`,`location_id`,`table_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20011 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_reviews`
--

CREATE TABLE IF NOT EXISTS `hangry_reviews` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `sale_type` varchar(32) NOT NULL DEFAULT '',
  `author` varchar(32) NOT NULL,
  `location_id` int(11) NOT NULL,
  `quality` int(11) NOT NULL,
  `delivery` int(11) NOT NULL,
  `service` int(11) NOT NULL,
  `review_text` text NOT NULL,
  `date_added` datetime NOT NULL,
  `review_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`review_id`,`sale_type`,`sale_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_security_questions`
--

CREATE TABLE IF NOT EXISTS `hangry_security_questions` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `priority` tinyint(1) NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `hangry_security_questions`
--

INSERT INTO `hangry_security_questions` (`question_id`, `text`, `priority`) VALUES
(11, 'Whats your pets name?', 1),
(12, 'What high school did you attend?', 2),
(13, 'What is your father''s middle name?', 7),
(14, 'What is your mother''s name?', 3),
(15, 'What is your place of birth?', 4),
(16, 'Whats your favourite teacher''s name?', 5);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_settings`
--

CREATE TABLE IF NOT EXISTS `hangry_settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `sort` varchar(45) NOT NULL,
  `item` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `serialized` tinyint(1) NOT NULL,
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `item` (`item`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11534 ;

--
-- Dumping data for table `hangry_settings`
--

INSERT INTO `hangry_settings` (`setting_id`, `sort`, `item`, `value`, `serialized`) VALUES
(7870, 'prefs', 'mail_template_id', '11', 0),
(8500, 'ratings', 'ratings', 'a:1:{s:7:"ratings";a:5:{i:1;s:3:"Bad";i:2;s:5:"Worse";i:3;s:4:"Good";i:4;s:7:"Average";i:5;s:9:"Excellent";}}', 1),
(10972, 'prefs', 'ti_setup', 'installed', 0),
(10975, 'prefs', 'ti_version', '2.1.1', 0),
(10976, 'prefs', 'last_version_check', 'a:2:{s:18:"last_version_check";s:19:"28-08-2016 11:02:46";s:4:"core";N;}', 1),
(10981, 'prefs', 'default_themes', 'a:3:{s:5:"admin";s:18:"tastyigniter-blue/";s:4:"main";s:26:"tastyigniter-orange-child/";s:11:"main_parent";s:20:"tastyigniter-orange/";}', 1),
(11050, 'prefs', 'default_location_id', '11', 0),
(11397, 'prefs', 'active_theme_options', 'a:1:{s:4:"main";a:2:{i:0;s:25:"tastyigniter-orange-child";i:1;a:20:{s:14:"display_crumbs";s:1:"0";s:15:"hide_admin_link";s:1:"1";s:16:"ga_tracking_code";s:0:"";s:4:"font";a:5:{s:6:"family";s:25:""Oxygen",Arial,sans-serif";s:6:"weight";s:6:"normal";s:5:"style";s:6:"normal";s:4:"size";s:2:"13";s:5:"color";s:7:"#333333";}s:9:"menu_font";a:5:{s:6:"family";s:25:""Oxygen",Arial,sans-serif";s:6:"weight";s:6:"normal";s:5:"style";s:6:"normal";s:4:"size";s:2:"16";s:5:"color";s:7:"#ed561a";}s:4:"body";a:6:{s:10:"background";s:7:"#ffffff";s:5:"image";s:0:"";s:7:"display";s:7:"contain";s:10:"foreground";s:7:"#ffffff";s:5:"color";s:7:"#ed561a";s:6:"border";s:7:"#dddddd";}s:4:"link";a:2:{s:5:"color";s:7:"#337ab7";s:5:"hover";s:7:"#23527c";}s:7:"heading";a:6:{s:10:"background";s:0:"";s:5:"image";s:0:"";s:7:"display";s:7:"contain";s:5:"color";s:7:"#333333";s:11:"under_image";s:0:"";s:12:"under_height";s:2:"50";}s:6:"button";a:6:{s:7:"default";a:3:{s:10:"background";s:7:"#e7e7e7";s:5:"hover";s:7:"#e7e7e7";s:4:"font";s:7:"#333333";}s:7:"primary";a:3:{s:10:"background";s:7:"#428bca";s:5:"hover";s:7:"#428bca";s:4:"font";s:7:"#ffffff";}s:7:"success";a:3:{s:10:"background";s:7:"#5cb85c";s:5:"hover";s:7:"#5cb85c";s:4:"font";s:7:"#ffffff";}s:4:"info";a:3:{s:10:"background";s:7:"#5bc0de";s:5:"hover";s:7:"#5bc0de";s:4:"font";s:7:"#ffffff";}s:7:"warning";a:3:{s:10:"background";s:7:"#f0ad4e";s:5:"hover";s:7:"#f0ad4e";s:4:"font";s:7:"#ffffff";}s:6:"danger";a:3:{s:10:"background";s:7:"#d9534f";s:6:"border";s:7:"#d9534f";s:4:"font";s:7:"#ffffff";}}s:7:"sidebar";a:5:{s:10:"background";s:7:"#ffffff";s:5:"image";s:0:"";s:7:"display";s:7:"contain";s:4:"font";s:7:"#484848";s:6:"border";s:7:"#eeeeee";}s:6:"header";a:5:{s:10:"background";s:7:"#ffffff";s:5:"image";s:0:"";s:7:"display";s:7:"contain";s:19:"dropdown_background";s:7:"#ed561a";s:5:"color";s:7:"#ed561a";}s:10:"logo_image";s:0:"";s:9:"logo_text";s:0:"";s:11:"logo_height";s:2:"40";s:16:"logo_padding_top";s:2:"10";s:19:"logo_padding_bottom";s:2:"10";s:7:"favicon";s:0:"";s:6:"footer";a:8:{s:10:"background";s:7:"#ed561a";s:5:"image";s:0:"";s:7:"display";s:7:"contain";s:17:"bottom_background";s:7:"#ed561a";s:12:"bottom_image";s:0:"";s:14:"bottom_display";s:7:"contain";s:12:"footer_color";s:7:"#ffffff";s:19:"bottom_footer_color";s:7:"#ffffff";}s:6:"social";a:12:{s:8:"facebook";s:1:"#";s:7:"twitter";s:1:"#";s:6:"google";s:1:"#";s:7:"youtube";s:1:"#";s:5:"vimeo";s:0:"";s:8:"linkedin";s:0:"";s:9:"pinterest";s:0:"";s:6:"tumblr";s:0:"";s:6:"flickr";s:0:"";s:9:"instagram";s:0:"";s:8:"dribbble";s:0:"";s:10:"foursquare";s:0:"";}s:13:"custom_script";a:3:{s:3:"css";s:127:"#main-header .navbar-nav > li > a:focus, #main-header .navbar-nav > li > a:hover, .local-box-fluid h2 {color: #fff !important;}";s:4:"head";s:0:"";s:6:"footer";s:0:"";}}}}', 1),
(11466, 'prefs', 'main_address', 'a:14:{s:11:"location_id";s:2:"11";s:13:"location_name";s:6:"Mumbai";s:9:"address_1";s:10:"Demo add 1";s:9:"address_2";s:10:"Demo add 2";s:4:"city";s:6:"Mumbai";s:5:"state";s:11:"Maharashtra";s:8:"postcode";s:6:"400056";s:10:"country_id";s:2:"99";s:7:"country";s:5:"India";s:10:"iso_code_2";s:2:"IN";s:10:"iso_code_3";s:3:"IND";s:12:"location_lat";s:9:"19.076000";s:12:"location_lng";s:9:"72.877701";s:6:"format";s:0:"";}', 1),
(11467, 'config', 'site_name', 'Hangry Hours', 0),
(11468, 'config', 'site_email', 'smartynirmal@gmail.com', 0),
(11469, 'config', 'site_logo', 'data/Logomakr_6ufU9b.png', 0),
(11470, 'config', 'country_id', '99', 0),
(11471, 'config', 'timezone', 'Asia/Kolkata', 0),
(11472, 'config', 'date_format', '%j%S %F %Y', 0),
(11473, 'config', 'time_format', '%h:%i %A', 0),
(11474, 'config', 'currency_id', '98', 0),
(11475, 'config', 'auto_update_currency_rates', '0', 0),
(11476, 'config', 'accepted_currencies', 'a:1:{i:0;s:2:"98";}', 1),
(11477, 'config', 'detect_language', '0', 0),
(11478, 'config', 'language_id', 'english', 0),
(11479, 'config', 'admin_language_id', 'english', 0),
(11480, 'config', 'customer_group_id', '11', 0),
(11481, 'config', 'page_limit', '20', 0),
(11482, 'config', 'meta_description', 'Hangry Hours', 0),
(11483, 'config', 'meta_keywords', 'Hangry Hours', 0),
(11484, 'config', 'menus_page_limit', '20', 0),
(11485, 'config', 'show_menu_images', '1', 0),
(11486, 'config', 'menu_images_h', '80', 0),
(11487, 'config', 'menu_images_w', '95', 0),
(11488, 'config', 'special_category_id', '11', 0),
(11489, 'config', 'tax_mode', '0', 0),
(11490, 'config', 'tax_percentage', '', 0),
(11491, 'config', 'tax_menu_price', '0', 0),
(11492, 'config', 'tax_delivery_charge', '0', 0),
(11493, 'config', 'stock_checkout', '0', 0),
(11494, 'config', 'show_stock_warning', '1', 0),
(11495, 'config', 'registration_terms', '0', 0),
(11496, 'config', 'checkout_terms', '0', 0),
(11497, 'config', 'maps_api_key', 'AIzaSyCRcPnZZgLSegMc5r8xaragrHYJg6lO4fs', 0),
(11498, 'config', 'distance_unit', 'km', 0),
(11499, 'config', 'future_orders', '0', 0),
(11500, 'config', 'location_order', '1', 0),
(11501, 'config', 'allow_reviews', '0', 0),
(11502, 'config', 'approve_reviews', '1', 0),
(11503, 'config', 'default_order_status', '11', 0),
(11504, 'config', 'processing_order_status', 'a:3:{i:0;s:2:"12";i:1;s:2:"13";i:2;s:2:"14";}', 1),
(11505, 'config', 'completed_order_status', 'a:1:{i:0;s:2:"15";}', 1),
(11506, 'config', 'canceled_order_status', '19', 0),
(11507, 'config', 'auto_invoicing', '0', 0),
(11508, 'config', 'invoice_prefix', 'INV-{year}-00', 0),
(11509, 'config', 'guest_order', '1', 0),
(11510, 'config', 'delivery_time', '45', 0),
(11511, 'config', 'collection_time', '15', 0),
(11512, 'config', 'reservation_mode', '0', 0),
(11513, 'config', 'default_reservation_status', '18', 0),
(11514, 'config', 'confirmed_reservation_status', '16', 0),
(11515, 'config', 'canceled_reservation_status', '17', 0),
(11516, 'config', 'reservation_time_interval', '45', 0),
(11517, 'config', 'reservation_stay_time', '60', 0),
(11518, 'config', 'image_manager', 'a:11:{s:8:"max_size";s:4:"3000";s:11:"thumb_width";s:3:"320";s:12:"thumb_height";s:3:"220";s:7:"uploads";s:1:"1";s:10:"new_folder";s:1:"1";s:4:"copy";s:1:"1";s:4:"move";s:1:"1";s:6:"rename";s:1:"1";s:6:"delete";s:1:"1";s:15:"transliteration";s:1:"0";s:13:"remember_days";s:1:"7";}', 1),
(11519, 'config', 'registration_email', 'a:1:{i:0;s:8:"customer";}', 1),
(11520, 'config', 'order_email', 'a:1:{i:0;s:8:"customer";}', 1),
(11521, 'config', 'reservation_email', 'a:1:{i:0;s:8:"customer";}', 1),
(11522, 'config', 'protocol', 'mail', 0),
(11523, 'config', 'smtp_host', '', 0),
(11524, 'config', 'smtp_port', '', 0),
(11525, 'config', 'smtp_user', 'root', 0),
(11526, 'config', 'smtp_pass', 'root', 0),
(11527, 'config', 'customer_online_time_out', '120', 0),
(11528, 'config', 'customer_online_archive_time_out', '0', 0),
(11529, 'config', 'permalink', '1', 0),
(11530, 'config', 'maintenance_mode', '0', 0),
(11531, 'config', 'maintenance_message', 'Site is under maintenance. Please check back later.', 0),
(11532, 'config', 'cache_mode', '0', 0),
(11533, 'config', 'cache_time', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_staffs`
--

CREATE TABLE IF NOT EXISTS `hangry_staffs` (
  `staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_name` varchar(32) NOT NULL,
  `staff_email` varchar(96) NOT NULL,
  `staff_group_id` int(11) NOT NULL,
  `staff_location_id` int(11) NOT NULL,
  `timezone` varchar(32) NOT NULL,
  `language_id` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `staff_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`staff_id`),
  UNIQUE KEY `staff_email` (`staff_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `hangry_staffs`
--

INSERT INTO `hangry_staffs` (`staff_id`, `staff_name`, `staff_email`, `staff_group_id`, `staff_location_id`, `timezone`, `language_id`, `date_added`, `staff_status`) VALUES
(11, 'Nirmal', 'smartynirmal@gmail.com', 11, 0, '0', 11, '2016-08-28', 1),
(12, 'Arpit', 'arpit.tolia@gmail.com', 12, 0, '', 0, '2016-08-28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_staff_groups`
--

CREATE TABLE IF NOT EXISTS `hangry_staff_groups` (
  `staff_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_group_name` varchar(32) NOT NULL,
  `customer_account_access` tinyint(4) NOT NULL,
  `location_access` tinyint(1) NOT NULL,
  `permissions` text NOT NULL,
  PRIMARY KEY (`staff_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `hangry_staff_groups`
--

INSERT INTO `hangry_staff_groups` (`staff_group_id`, `staff_group_name`, `customer_account_access`, `location_access`, `permissions`) VALUES
(11, 'Administrator', 1, 0, 'a:46:{i:11;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:12;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:13;a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}i:14;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:15;a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}i:16;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:17;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:18;a:1:{i:0;s:6:"access";}i:19;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:20;a:2:{i:0;s:6:"access";i:1;s:6:"delete";}i:21;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:22;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:25;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:26;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:27;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:28;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:29;a:2:{i:0;s:3:"add";i:1;s:6:"delete";}i:30;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:32;a:3:{i:0;s:6:"access";i:1;s:3:"add";i:2;s:6:"delete";}i:33;a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}i:34;a:2:{i:0;s:3:"add";i:1;s:6:"delete";}i:35;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:36;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:37;a:2:{i:0;s:3:"add";i:1;s:6:"delete";}i:39;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:40;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:41;a:2:{i:0;s:6:"access";i:1;s:6:"manage";}i:42;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:43;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:23;a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}i:24;a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}i:31;a:3:{i:0;s:6:"manage";i:1;s:3:"add";i:2;s:6:"delete";}i:38;a:1:{i:0;s:6:"manage";}i:44;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}i:45;a:1:{i:0;s:6:"manage";}i:46;a:1:{i:0;s:6:"manage";}i:47;a:1:{i:0;s:6:"manage";}i:48;a:1:{i:0;s:6:"manage";}i:49;a:1:{i:0;s:6:"manage";}i:50;a:1:{i:0;s:6:"manage";}i:51;a:1:{i:0;s:6:"manage";}i:52;a:1:{i:0;s:6:"manage";}i:53;a:1:{i:0;s:6:"manage";}i:54;a:1:{i:0;s:6:"manage";}i:55;a:1:{i:0;s:3:"add";}i:56;a:4:{i:0;s:6:"access";i:1;s:6:"manage";i:2;s:3:"add";i:3;s:6:"delete";}}'),
(12, 'Manager', 0, 1, 'a:7:{i:12;a:3:{i:0;s:6:"access";i:1;s:3:"add";i:2;s:6:"delete";}i:22;a:3:{i:0;s:6:"access";i:1;s:3:"add";i:2;s:6:"delete";}i:27;a:3:{i:0;s:6:"access";i:1;s:3:"add";i:2;s:6:"delete";}i:28;a:3:{i:0;s:6:"access";i:1;s:3:"add";i:2;s:6:"delete";}i:30;a:3:{i:0;s:6:"access";i:1;s:3:"add";i:2;s:6:"delete";}i:36;a:1:{i:0;s:6:"access";}i:40;a:1:{i:0;s:6:"access";}}');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_statuses`
--

CREATE TABLE IF NOT EXISTS `hangry_statuses` (
  `status_id` int(15) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(45) NOT NULL,
  `status_comment` text NOT NULL,
  `notify_customer` tinyint(1) NOT NULL,
  `status_for` varchar(10) NOT NULL,
  `status_color` varchar(32) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `hangry_statuses`
--

INSERT INTO `hangry_statuses` (`status_id`, `status_name`, `status_comment`, `notify_customer`, `status_for`, `status_color`) VALUES
(11, 'Received', 'Your order has been received.', 1, 'order', '#686663'),
(12, 'Pending', 'Your order is pending', 1, 'order', '#f0ad4e'),
(13, 'Preparation', 'Your order is in the kitchen', 1, 'order', '#00c0ef'),
(14, 'Delivery', 'Your order will be with you shortly.', 0, 'order', '#00a65a'),
(15, 'Completed', '', 0, 'order', '#00a65a'),
(16, 'Confirmed', 'Your table reservation has been confirmed.', 0, 'reserve', '#00a65a'),
(17, 'Canceled', 'Your table reservation has been canceled.', 0, 'reserve', '#dd4b39'),
(18, 'Pending', 'Your table reservation is pending.', 0, 'reserve', ''),
(19, 'Canceled', '', 0, 'order', '#ea0b29');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_status_history`
--

CREATE TABLE IF NOT EXISTS `hangry_status_history` (
  `status_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `assignee_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `notify` tinyint(1) NOT NULL,
  `status_for` varchar(32) NOT NULL,
  `comment` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`status_history_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_tables`
--

CREATE TABLE IF NOT EXISTS `hangry_tables` (
  `table_id` int(11) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(32) NOT NULL,
  `min_capacity` int(11) NOT NULL,
  `max_capacity` int(11) NOT NULL,
  `table_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

-- --------------------------------------------------------

--
-- Table structure for table `hangry_uri_routes`
--

CREATE TABLE IF NOT EXISTS `hangry_uri_routes` (
  `uri_route_id` int(11) NOT NULL AUTO_INCREMENT,
  `uri_route` varchar(255) NOT NULL,
  `controller` varchar(128) NOT NULL,
  `priority` tinyint(11) NOT NULL,
  PRIMARY KEY (`uri_route_id`,`uri_route`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `hangry_uri_routes`
--

INSERT INTO `hangry_uri_routes` (`uri_route_id`, `uri_route`, `controller`, `priority`) VALUES
(1, 'locations', 'local/locations', 1),
(2, 'account', 'account/account', 2),
(3, '(:any)', 'pages', 3);

-- --------------------------------------------------------

--
-- Table structure for table `hangry_users`
--

CREATE TABLE IF NOT EXISTS `hangry_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(9) NOT NULL,
  PRIMARY KEY (`user_id`,`staff_id`,`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `hangry_users`
--

INSERT INTO `hangry_users` (`user_id`, `staff_id`, `username`, `password`, `salt`) VALUES
(11, 11, 'admin', '1910bf5ba1ae173d305d735e6142e3176e23a39b', '75baa3aa7'),
(12, 12, 'arpit', '900720730993361767b011d37323b7df48c88673', 'c7986098f');

-- --------------------------------------------------------

--
-- Table structure for table `hangry_working_hours`
--

CREATE TABLE IF NOT EXISTS `hangry_working_hours` (
  `location_id` int(11) NOT NULL,
  `weekday` int(11) NOT NULL,
  `opening_time` time NOT NULL DEFAULT '00:00:00',
  `closing_time` time NOT NULL DEFAULT '00:00:00',
  `status` tinyint(1) NOT NULL,
  `type` varchar(32) NOT NULL,
  PRIMARY KEY (`location_id`,`weekday`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hangry_working_hours`
--

INSERT INTO `hangry_working_hours` (`location_id`, `weekday`, `opening_time`, `closing_time`, `status`, `type`) VALUES
(11, 0, '19:00:00', '01:00:00', 1, 'collection'),
(11, 0, '19:00:00', '01:00:00', 1, 'delivery'),
(11, 0, '19:00:00', '01:00:00', 1, 'opening'),
(11, 1, '19:00:00', '01:00:00', 1, 'collection'),
(11, 1, '19:00:00', '01:00:00', 1, 'delivery'),
(11, 1, '19:00:00', '01:00:00', 1, 'opening'),
(11, 2, '19:00:00', '01:00:00', 1, 'collection'),
(11, 2, '19:00:00', '01:00:00', 1, 'delivery'),
(11, 2, '19:00:00', '01:00:00', 1, 'opening'),
(11, 3, '19:00:00', '01:00:00', 1, 'collection'),
(11, 3, '19:00:00', '01:00:00', 1, 'delivery'),
(11, 3, '19:00:00', '01:00:00', 1, 'opening'),
(11, 4, '19:00:00', '01:00:00', 1, 'collection'),
(11, 4, '19:00:00', '01:00:00', 1, 'delivery'),
(11, 4, '19:00:00', '01:00:00', 1, 'opening'),
(11, 5, '19:00:00', '01:00:00', 1, 'collection'),
(11, 5, '19:00:00', '01:00:00', 1, 'delivery'),
(11, 5, '19:00:00', '01:00:00', 1, 'opening'),
(11, 6, '19:00:00', '01:00:00', 1, 'collection'),
(11, 6, '19:00:00', '01:00:00', 1, 'delivery'),
(11, 6, '19:00:00', '01:00:00', 1, 'opening');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
